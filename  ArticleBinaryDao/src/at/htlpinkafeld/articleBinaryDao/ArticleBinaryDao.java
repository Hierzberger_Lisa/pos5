package at.htlpinkafeld.articleBinaryDao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleBinaryDao implements BaseDao<Article> {

    private String outputFile;
    private String inputFile;

    public ArticleBinaryDao(String outputFile, String inputFile) {  //Constructor
        this.outputFile = outputFile;
        this.inputFile = inputFile;
        }

    @Override
    public void write(List<Article> list) throws IOException, EOFException {
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile)));

        try {
            for (Article article : list) {
                out.writeInt(article.getId());
                out.writeUTF(article.getName());
                out.writeDouble(article.getPrice());
            }
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    public List<Article> read() throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(inputFile)));
        List<Article> returnList = new ArrayList<>();

        try {
            while (true) {
                returnList.add(new Article(in.readInt(), in.readUTF(), in.readDouble()));
            }
        } finally {
            if (in != null) {
                in.close();
            }
            return returnList;
        }

    }
}
