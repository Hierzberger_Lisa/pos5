/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.articleBinaryDao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lisa
 */
public class ArticleBinaryDemo {
    public static void main(String[] args) throws IOException {
        ArticleBinaryDao articleBinaryDemo = new ArticleBinaryDao("./outputFile", "./outputFile");
        
        List<Article> articleList = new ArrayList<>(); 
        articleList.add(new Article(1, "Hose", 69.99));
        articleList.add(new Article(2, "T-Shirt", 29.99));
        articleList.add(new Article(3, "Müze", 39.99));
        
        articleBinaryDemo.write(articleList);

        List<Article> articleList1 = articleBinaryDemo.read();

        System.out.println(articleList1.toArray());

    }
}