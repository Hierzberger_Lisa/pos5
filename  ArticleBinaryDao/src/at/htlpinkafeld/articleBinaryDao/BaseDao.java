/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.articleBinaryDao;

import java.io.IOException;
import java.util.List;


/**
 *
 * @author Lisa
 */

public interface BaseDao<T> {
    public void write(List<T> list) throws IOException;
    public List<T> read() throws IOException;
}
