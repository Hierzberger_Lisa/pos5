/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class AccountProxy implements AccountInterface {

    private String eigenschaft;
    private Account realAccount;

    public AccountProxy(String eigenschaft, Account realAccount) {
        this.eigenschaft = eigenschaft;
        this.realAccount = realAccount;
    }

    @Override
    public void setBalance(int b) {
        if (this.eigenschaft == "Admin") {
            this.realAccount.setBalance(b);
        }
        else{
            System.out.println("no access");
        }
    }

    @Override
    public int getBalance() {
        if (this.eigenschaft == "Admin" || this.eigenschaft == "Owner") {
            return this.realAccount.getBalance();
        } else {
            return -1;
        }
    }

}
