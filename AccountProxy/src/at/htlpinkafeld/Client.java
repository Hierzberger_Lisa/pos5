/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class Client {

    public static void main(String[] args) {
        Account account1 = new Account(1, "Susi", 1500);
        AccountInterface a = new AccountProxy("Admin", account1);
        int y = a.getBalance();
        System.out.println("Your Balance is " + y);

        Account a2 = new Account(2, "Peter", 2000);
        AccountInterface ai = new AccountProxy("Owner", a2);
        int i = 0;
        i = ai.getBalance();
        System.out.println(i);

        a2.setBalance(5000);
        a.setBalance(100);

        y = a.getBalance();
        System.out.println(y);
        
        i = ai.getBalance();
        System.out.println(i);

    }
}
