/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import POJO.Article;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author quat
 */
public class main {
    
    public static void main(String[] args) {
        List<Article> articles = new ArrayList();
        articles.add(new Article("Canon EF-M 22m f/2",
                Article.Category.PHOTO, 99.90));
        articles.add(new Article("AMD Ryzen 7 3700x",
                Article.Category.HARDWARE, 329.00));        
        articles.add(new Article("Nintendo Switch",
                Article.Category.GAMES, 315.50));        
        System.out.println("1-1");
        //print all articles
        processAll(articles,
                a -> true,
                a -> {
                },
                System.out::println
        );
        System.out.println("2-1");
        //reduce the price of all games by 20% and print all modified articles
        processAll(articles,
                a -> a.getCategory() == Article.Category.GAMES,
                a -> a.setPrice(a.getPrice() * 0.8),
                System.out::println
        );
        System.out.println("3-1");
        //print all articles below 100€ "category | name | price"
        processAll(articles,
                a -> a.getPrice() < 100,
                a -> {
                },
                a -> System.out.println(a.getCategory() + " | "
                        + a.getName() + " | "
                        + a.getPrice())
        );
        System.out.println("1-2");
        //print all articles below 100€ "category | name | price"
        processAll2(articles,
                a -> true,
                a -> a,
                System.out::println
        );
        System.out.println("2-2");
        //print all articles below 100€ "category | name | price"
        processAll2(articles,
                a -> a.getCategory() == Article.Category.GAMES,
                a ->  a.getName(),
                System.out::println

        );
        System.out.println("2-3");
        //print all articles below 100€ "category | name | price"
        processAll2(articles,
                a -> a.getPrice() < 100,
                a -> a,
                a -> System.out.println(a.getCategory() + " | "
                        + a.getName() + " | "
                        + a.getPrice())
        );
        
        
        
    }
    
    public void reducePrice(List<Article> articles,
            Article.Category cat, double percentage) {

        /* articles.stream().filter(
                (a)->{
                    return (a.getCategory() == cat);
                }).peek(
                (a)->{
                    a.setPrice(a.getPrice()*percentage);
                }).forEach(
                (a)->{
                    System.out.println(a);
                });*/
        articles.stream()
                .filter(a -> a.getCategory() == cat)
                .peek(a -> a.setPrice(a.getPrice() * percentage))
                .forEach(System.out::println);

        /* articles.stream()
                .filter(new Predicate<Article>() {
                    @Override
                    public boolean test(Article t) {
                        return t.getCategory() == cat;
                    }
                }).peek(new Consumer<Article>() {
                    @Override
                    public void accept(Article t) {
                        t.setPrice(t.getPrice() * percentage);
                    }
                }).forEach(new Consumer<Article>() {
                    @Override
                    public void accept(Article t) {
                        System.out.println(t.toString());
                    }
                });*/
    }
    
    public static <T> void processAll(Collection<T> source,
            Predicate<T> filter,
            Consumer<T> process,
            Consumer<T> print) {
        for (T t : source) {
            if (filter.test(t)) {
                process.accept(t);
                print.accept(t);
            }
        }
    }
    
    public static <T, R> void processAll2(Collection<T> source,
            Predicate<T> filter,
            Function<T, R> mapper,
            Consumer<R> printer) {
        
        for (T t : source) {
            if (filter.test(t)) {
                R r;
                r = mapper.apply(t);
                printer.accept(r);
            }
        }
    }
    
}
