/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

import at.htlpinkafeld.dao.ArticleTextDao;
import at.htlpinkafeld.pojo.Article;

import java.io.IOException;
import java.util.List;

/**
 *
 * @author Lisa
 */
// Main
public class ArticleTextDemo {
    
    public static void main(String[] args) throws IOException {
        ArticleTextDao atd = new ArticleTextDao("./outputFile.txt", "./inputFile.txt");

        List<Article> articleList = atd.read();

        atd.write(articleList);
    }
}
