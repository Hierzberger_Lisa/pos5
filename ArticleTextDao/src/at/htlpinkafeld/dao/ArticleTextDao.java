/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.dao;

import at.htlpinkafeld.interfaces.BaseDao;
import at.htlpinkafeld.pojo.Article;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lisa
 */

public class ArticleTextDao implements BaseDao<Article> {
    private String outputFile;
    private String inputFile;

    public ArticleTextDao(String outputFile, String inputFile) {
        this.outputFile = outputFile;
        this.inputFile = inputFile;
    }

    @Override
    public void write(List<Article> list) throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(outputFile));

        try {
            for(Article article : list){
                out.println(article.toString());
               
            }
        }finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @Override
    public List<Article> read() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(inputFile));;
        List<Article> returnList = new ArrayList<>();

        try{
            String inputLine;

            while((inputLine = in.readLine()) != null){
                String[] splittedString = inputLine.split(",");
                returnList.add(new Article(Integer.valueOf(splittedString[0]), splittedString[1], Double.valueOf(splittedString[2])));
            }
        }finally {
            if(in != null){
                in.close();
            }
        }
        return returnList;
    }
}
