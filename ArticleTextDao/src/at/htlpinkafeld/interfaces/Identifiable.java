package at.htlpinkafeld.interfaces;

public interface Identifiable {
    public int getId();
    public void setId(int id);
}
