/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Haspl
 */
class Calculator {
    Map<String, Calculatable> ops = new HashMap<>();
    
    public void addOp(String op, Calculatable c){
        ops.put(op, c);
    }
        

    double calculate(String op, double a, double b) {
        return ops.get(op).calculate(a, b);
    }
    
}
