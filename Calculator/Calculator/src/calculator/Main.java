/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Haspl
 */
public class Main {
    public static void main(String[] args){
        Calculator c = new Calculator();
        c.addOp("+", new Add());
        c.addOp("-", (x,y) -> x-y);
    }
}
