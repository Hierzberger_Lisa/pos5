/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardecorator;

/**
 *
 * @author Lisa
 **/


public class CarDecorator implements Car {
    private Car c;
    
    public CarDecorator(Car c) {
        this.c = c;
    }
    
    @Override
    public void assemble() {
        c.assemble();
    }
}
