/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardecorator;

/**
 *
 * @author Lisa
 */
public class Main {

    public static void main(String[] args) {
        Car c = new LuxuryCar(new BasicCar());
        c.assemble();
        System.out.println("\n");
        
      
        Car c2 = new SportsCar(new BasicCar());
        c2.assemble();
    }
}
