package at.htlpinkafeld.reader;


import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Lisa
 */

//Testverbesserung
public class CharacterCountReader extends FilterReader {

    private char toCount;
    private int counter = 0;

    public CharacterCountReader(Reader in, char toCount) {
        super(in);
        this.toCount = toCount;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int charsRead = 0;
        int x = 0;
        for (int i = off; i < len + off; i++) {
            x = this.read();
            if (x != -1) {
                charsRead++;
                cbuf[i] = (char) x;
            }
        }
        if (charsRead == 0) {
            charsRead = -1;
        }
        return charsRead;
    }

    @Override
    public int read() throws IOException {
        int read = super.read();
        if (toCount == read) {
            counter++;
        }
        return read;
    }

}
