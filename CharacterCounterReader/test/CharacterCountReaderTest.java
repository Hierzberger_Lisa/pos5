/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import at.htlpinkafeld.reader.CharacterCountReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lisa
 */
public class CharacterCountReaderTest {

    public CharacterCountReaderTest() {
    }

    @Test
    public void testCountCharacters() throws IOException {
        CharacterCountReader ccr = new CharacterCountReader(new StringReader("a.b.c."), '.');
        BufferedReader br = new BufferedReader(ccr);

        String readString = br.readLine();

        assertEquals(2, ccr.getCounter());
        assertEquals("a.b.c", readString);
    }

}
