/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.christmastreedecorator;

/**
 *
 * @author Sophie
 */
public class Garland extends TreeDecorator{
    public Garland(ChristmasTree tree) {
        super(tree);
    }
     
    public String decorate() {
        return super.decorate() + decorateWithGarland();
    }
     
    private String decorateWithGarland() {
        return " with Garland";
    }
}
