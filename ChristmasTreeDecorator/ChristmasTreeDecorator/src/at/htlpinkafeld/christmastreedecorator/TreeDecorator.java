/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.christmastreedecorator;

/**
 *
 * @author Sophie
 */
public class TreeDecorator implements ChristmasTree{
    private ChristmasTree tree;
    
    public TreeDecorator(ChristmasTree tree){
        this.tree=tree;
    }
    
    @Override
    public String decorate() {
        return tree.decorate();
    }
    
}
