/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.christmastreedecorator;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Sophie
 */
public class ChristmasTreeDecoratorTest {

    @Test
    public void testMain() {
        ChristmasTree tree1 = new Garland(new ChristmasTreeImpl());
        assertEquals(tree1.decorate(), "Christmas tree with Garland");
    }
}
