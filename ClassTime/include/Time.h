#ifndef TIME_TIME_H
#define TIME_TIME_H
#include <iostream>

using namespace std;

class Time {
    enum Vor {POS, NEG};

private:
    int hours;
    int minutes;
    double seconds;
    Vor vor;

public:

    enum Unit{HOUR, MIN, SEC};
    Time(double seconds=0.0, int minutes=0, int hours=0);
    ~Time();

    void show() const;
    void validate();
    double getTime(Unit unit = Time::SEC) const;
    void setTime(double d, Unit unit = Time::SEC);

    int getHours() const;
    void setHours(int hours);
    int getMinutes() const;
    void setMinutes(int minutes);
    double getSeconds() const;
    void setSeconds(double seconds);

    string toString() const;

    Time& operator++ (); //pre
    Time  operator++ (int); //post

    friend std::ostream &operator<<(std::ostream &os, const Time &time);

    bool operator==(const Time &rhs) const;
    bool operator==(const double time) const;
    friend bool operator==(const double i, const Time &rhs);

    bool operator!=(const Time &rhs) const;

    bool operator<(const Time &rhs) const;
    friend bool operator<(const double i, const Time &rhs);

    Time operator+=(const Time &rhs);
    friend Time operator+(double val, const Time &rhs);

};


#endif //TIME_TIME_H
