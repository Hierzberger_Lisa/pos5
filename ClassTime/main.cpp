#include <iostream>
#include "Time.h"

using namespace std;

int main() {
    Time tOne(58.25, 59, 0);
    cout << "Initial value of timeOne: " << endl << "Show:     ";
    tOne.show();
    cout << endl << "op<<:     " << tOne << endl;
    cout << "toString: " << tOne.toString() << endl << endl;

    cout << "++timeOne: " << ++tOne << endl;
    cout << "timeOne++: " << tOne++ << endl;
    cout << "timeOne:   " << tOne << endl;

    cout << "timeOne+=timeOne: " << (tOne+=tOne) << endl;
    cout << "timeOne:          " << tOne << endl;

    Time tTwo;
    tTwo = 2.0 + tOne;
    cout << endl << "timeTwo = 2 + timeOne" << endl;
    cout << "timeTwo: " << tTwo << endl << endl;

    cout << "timeTwo < timeOne: " << (tTwo < tOne) << endl;
    cout << "timeOne < timeTwo: " << (tOne < tTwo) << endl;
    cout << "100 < timeOne:     " << (100.0 < tOne) << endl;
    cout << "timeOne < 100:     " << (tOne < 100) << endl << endl;

    tOne = 100.5;
    tTwo.setTime(100.5);
    cout << "timeOne, timeTwo:   " << tOne << ", " << tTwo << endl;
    cout << "timeOne == timeTwo: " << (tOne == tTwo) << endl;
    cout << "timeTwo == 100.5:   " << (tTwo == 100.5) << endl;
    cout << "100.5 == timeOne:   " << (100.5 == tOne) << endl;

    return 0;
}
