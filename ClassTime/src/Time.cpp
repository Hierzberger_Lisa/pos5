#include <stdio.h>
#include <cmath>
#include <sstream>
#include <iomanip>

#include "Time.h"

using namespace std;

Time::Time(double seconds, int minutes, int hours) : hours(hours), minutes(minutes), seconds(seconds) {}

Time::~Time(){}

int Time::getHours() const {
    return this->hours;
}

void Time::setHours(int hours) {
    this->hours = hours;
}

int Time::getMinutes() const {
    return this->minutes;
}

void Time::setMinutes(int minutes) {
   this->minutes = minutes;
}

double Time::getSeconds() const {
    return this->seconds;
}

void Time::setSeconds(double seconds) {
    this->seconds = seconds;
}

void Time::show() const{
    printf("%02d:%02d:%02.2f", this->hours, this->minutes, this->seconds);
}

void Time::validate(){
    if(this->vor == Time::NEG){
        this->seconds = fabs(this->seconds);
        this->minutes = fabs(this->minutes);
        this->hours = fabs(this->hours);
    }

    if(this->getSeconds()>=59){
        int counter = this->seconds / 60;
        this->minutes = this->minutes + counter;
        this->seconds = this->seconds - (60*counter);
    }
    if(this->minutes >= 59){
        int counter = this->minutes / 60;
        this->hours = this->hours + counter;
        this->minutes = this->minutes - (60*counter);
    }

}

double Time::getTime(Unit unit) const{

    if(unit == Time::SEC){
        return (this->getHours() *3600) + (this->getMinutes() * 60) + this->getSeconds();
    }
    if(unit == Time::MIN){
        return (this->getHours() * 60) + (this->getMinutes()) + (this->getSeconds() / 60);
    }
    if(unit == Time::HOUR){
        return (this->getHours()) + (this->getMinutes() /60) + (this->getSeconds() /3600);
    }
}

void Time::setTime(double d, Unit unit){
    switch (unit){
        case Time::HOUR:
            this->hours = 0;
            this->minutes = 0;
            this->seconds = d*3600;
            this->validate();
            break;
        case Time::MIN:
            this->hours = 0;
            this->minutes = 0;
            this->seconds = d * 60;
            this->validate();
            break;
        default:
            this->hours = 0;
            this->minutes = 0;
            this->seconds = d;
            this->validate();
            break;
    }

    this->validate();
}

string Time::toString() const{
    stringstream sStrm;
    sStrm << *this;
    return sStrm.str();
}

Time& Time::operator++()
{
    ++this->seconds;
    this->validate();
    return *this;
}

Time Time::operator++(int dummy)
{
    Time t(*this);
    ++this->seconds;
    this->validate();
    return t;
}

ostream &operator<<(ostream &os, const Time &time) {
    return os << setfill('0') << setw(2) << time.hours << ":" << setw(2) << time.minutes <<
              ":" << setw(2) << time.seconds;;
}

bool Time::operator==(const Time &rhs) const {
    return this->getTime() == rhs.getTime();
}

bool Time::operator==(const double time) const {
    return this->getTime() == time;
}

bool operator==(const double i, const Time &rhs){
    return i == rhs.getTime();
}

bool Time::operator!=(const Time &rhs) const {
    return !(rhs == *this);
}

bool Time::operator<(const Time &rhs) const {
    return this->getTime() < rhs.getTime();
}

bool operator<(const double i, const Time &rhs){
    return i < rhs.getTime();
}

Time Time::operator+=(const Time &rhs){
    this->setTime(this->getTime() + rhs.getTime(), Time::SEC);
    this->validate();
    return *this;
}

Time operator+(double val, const Time &rhs){
    Time time(rhs.getSeconds()+val, rhs.getMinutes(), rhs.getHours());
    time.validate();
    return time;
}


