/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.receiver;

import at.htlpinkafeld.interfaces.Showable;

/**
 *
 * @author Lisa
 */
public class VersionPrinter implements Showable{

    @Override
    public void showAll() {
        System.out.println("Allowed options are: -help, -version and -gui");
    }
    
}
