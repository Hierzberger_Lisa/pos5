/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.Client;

import at.htlpinkafeld.Commands.FanOffCommand;
import at.htlpinkafeld.Commands.FanOnCommand;
import at.htlpinkafeld.Commands.LightOffCommand;
import at.htlpinkafeld.Commands.LightOnCommand;
import at.htlpinkafeld.Invoker.Switch;
import at.htlpinkafeld.Receiver.Fan;
import at.htlpinkafeld.Receiver.Light;

/**
 *
 * @author Kathrin
 */
public class TestCommand {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Fan fan = new Fan();
        Light light = new Light();
        Switch lightSwitch = new Switch(new LightOnCommand(light), new LightOffCommand(light));
        Switch fanSwitch = new Switch(new FanOnCommand(fan), new FanOffCommand(fan));
        
        lightSwitch.flipUp();
        lightSwitch.flipDown();
        fanSwitch.flipUp();
        fanSwitch.flipDown();
          
        
    }
    
}
