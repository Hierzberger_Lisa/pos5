/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.Commands;

import at.htlpinkafeld.Receiver.Fan;

/**
 *
 * @author Kathrin
 */
public class FanOnCommand implements Command{

    private Fan fan;
    
    public FanOnCommand(Fan fan){
        this.fan=fan;
    }
    
    @Override
    public void execute() {
        this.fan.startRotating();
    }
    
}
