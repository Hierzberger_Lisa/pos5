/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.Invoker;

import at.htlpinkafeld.Commands.Command;

/**
 *
 * @author Kathrin
 */
public class Switch {
    
    private Command comOn;
    private Command comOff;
    
    public Switch(Command com1, Command com2){
        this.comOn = com1;
        this.comOff = com2;
    }
    
    public void flipUp(){
        comOn.execute();
    }
    
    public void flipDown(){
        comOff.execute();
    }
}
