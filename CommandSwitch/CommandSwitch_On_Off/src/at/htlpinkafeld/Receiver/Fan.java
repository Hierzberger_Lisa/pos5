/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.Receiver;

/**
 *
 * @author Kathrin
 */
public class Fan{
    private boolean status;
    
    public void startRotating(){
       this.status=true;
        System.out.println("Fan is rotating");
    }
    
    public void stopRotating(){
       this.status=false;
        System.out.println("Fan is not rotating");
    }

}
