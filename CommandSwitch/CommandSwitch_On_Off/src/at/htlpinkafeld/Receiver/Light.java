/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.Receiver;

/**
 *
 * @author Kathrin
 */
public class Light{
    private boolean status;
    
    public void turnOn(){
        this.status=true;
        System.out.println("Light is on");
    }
    
    public void turnOff(){
        this.status=false;
        System.out.println("Light is off");
    }
}
