package at.htlpinkafeld.Client;

import at.htlpinkafeld.Commands.FanOffCommand;
import at.htlpinkafeld.Commands.FanOnCommand;
import at.htlpinkafeld.Commands.LightOffCommand;
import at.htlpinkafeld.Commands.LightOnCommand;
import at.htlpinkafeld.Invoker.Switch;
import at.htlpinkafeld.Receiver.Fan;
import at.htlpinkafeld.Receiver.Light;

//Main
public class TestCommand {

    public static void main(String[] args) {
        
        Fan fan = new Fan();
        Light light = new Light();

        Switch lightSwitch = new Switch(new LightOnCommand(light), new LightOffCommand(light));
        lightSwitch.flipUp();
        lightSwitch.flipDown();
        
        Switch fanSwitch = new Switch(new FanOnCommand(fan), new FanOffCommand(fan));        
        fanSwitch.flipUp();
        fanSwitch.flipDown();
    }
}
