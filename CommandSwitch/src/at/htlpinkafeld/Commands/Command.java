package at.htlpinkafeld.Commands;

public interface Command {
    public void execute();
}
