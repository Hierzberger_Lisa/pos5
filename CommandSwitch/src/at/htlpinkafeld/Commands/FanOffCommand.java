package at.htlpinkafeld.Commands;

import at.htlpinkafeld.Receiver.Fan;

//Concrete Command
public class FanOffCommand implements Command {

    private Fan fan;

    public FanOffCommand(Fan f) {
        this.fan = f;
    }

    @Override
    public void execute() {
        this.fan.stopRotating();
    }

}
