package at.htlpinkafeld.Commands;

import at.htlpinkafeld.Receiver.Fan;


//Concrete Command
public class FanOnCommand implements Command {

    private Fan fan;

    public FanOnCommand(Fan f) {
        this.fan = f;
    }

    @Override
    public void execute() {
        this.fan.startRotating();
    }

}
