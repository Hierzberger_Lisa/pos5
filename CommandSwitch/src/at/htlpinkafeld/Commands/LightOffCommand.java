package at.htlpinkafeld.Commands;

import at.htlpinkafeld.Receiver.Light;

//Concrete Command
public class LightOffCommand implements Command {

    private Light light;

    public LightOffCommand(Light l) {
        this.light = l;
    }

    @Override
    public void execute() {
        this.light.turnOff();
    }

}
