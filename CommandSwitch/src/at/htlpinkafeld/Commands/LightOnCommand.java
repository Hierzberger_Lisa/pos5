package at.htlpinkafeld.Commands;

import at.htlpinkafeld.Receiver.Light;

//ConcreteCommand
public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light l) {
        this.light = l;
    }

    @Override
    public void execute() {
        this.light.turnOn();
    }

}
