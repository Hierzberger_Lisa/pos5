package at.htlpinkafeld.Invoker;

import at.htlpinkafeld.Commands.Command;

//Invoker
public class Switch {
    
    private Command on;
    private Command off;
    
    public Switch(Command on, Command off){
        this.on = on;
        this.off = off;
    }
    
    public void flipUp(){
        on.execute();
    }
    
    public void flipDown(){
        off.execute();
    }
}
