package at.htlpinkafeld.Receiver;

//Receiver
public class Fan {

    private boolean status;

    public void startRotating() {
        this.status = true;
        System.out.println("Fan is rotating");
    }

    public void stopRotating() {
        this.status = false;
        System.out.println("Fan is not rotating");
    }

}
