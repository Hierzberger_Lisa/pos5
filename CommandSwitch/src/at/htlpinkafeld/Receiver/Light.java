package at.htlpinkafeld.Receiver;

//Receiver
public class Light {

    private boolean status;

    public void turnOn() {
        this.status = true;
        System.out.println("Light is on");
    }

    public void turnOff() {
        this.status = false;
        System.out.println("Light is off");
    }
}
