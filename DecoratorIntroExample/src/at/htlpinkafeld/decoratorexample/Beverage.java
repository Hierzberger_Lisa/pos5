/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.decoratorexample;

/**
 *
 * @author Lisa
 */
public class Beverage {

    String description;
    private boolean milk;
    private boolean soy;
    private boolean mocha;
    private boolean whip;

    public static void main(String[] args) {
        Beverage hb = new HouseBlend();
        printBeverage(hb);
        
        System.out.println("            ");

        Beverage d = new Decaf();
        d.milk = true;
        printBeverage(d);
    }

    public String getDescription() {
        return this.description;
    }

    public double cost() {
        if (hasMilk()) {
            return 0.80;
        }

        if (hasSoy()) {
            return 1;
        }

        if (hasMocha()) {
            return 1.2;
        }

        if (hasWhip()) {
            return 1.5;
        }

        return 0;
    }

    public boolean hasMilk() {
        return milk;
    }

    public void setMilk(boolean milk) {
        this.milk = milk;
    }

    public boolean hasSoy() {
        return soy;
    }

    public void setSoy(boolean soy) {
        this.soy = soy;
    }

    public boolean hasMocha() {
        return mocha;
    }

    public void setMocha(boolean mocha) {
        this.mocha = mocha;
    }

    public boolean hasWhip() {
        return whip;
    }

    public void setWhip(boolean whip) {
        this.whip = whip;
    }

    private static void printBeverage(Beverage beverage) {
        System.out.println("Beverage: " + beverage.getDescription() + ", Cost: " + beverage.cost());
    }
}
