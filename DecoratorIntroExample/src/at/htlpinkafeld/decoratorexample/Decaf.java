/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.decoratorexample;

/**
 *
 * @author Lisa
 */
public class Decaf extends Beverage {

    public Decaf() {
        description = "Decaf";
    }

    public double cost() {
        return super.cost()+2.80;

    }
}