/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.decoratorexample;

/**
 *
 * @author Lisa
 */
public class HouseBlend extends Beverage {
      public HouseBlend() {
        description = "House Blend";
    }

    public double cost() {
       return super.cost()+1.90;
    }
}
