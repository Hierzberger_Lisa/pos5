package at.htlpinkafeld.daos;

import at.htlpinkafeld.pojos.Article;
import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;


public class ArticleCSVReader extends FilterReader{

    public ArticleCSVReader(Reader in) {
        super(in);
    }

    
    public Article readArticle() throws IOException{
        int input;
        StringBuilder sBuilder = new StringBuilder();
        
        while((input = in.read()) != ';'){
            char c = (char)input;
            sBuilder.append(c);
        }
        
        String[] strings = sBuilder.toString().split(", ");
        
        return new Article(Integer.parseInt(strings[0]), strings[1], Double.parseDouble(strings[2]));
    }
}
