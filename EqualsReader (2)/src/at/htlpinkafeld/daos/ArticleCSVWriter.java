package at.htlpinkafeld.daos;

import at.htlpinkafeld.pojos.Article;
import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

public class ArticleCSVWriter extends FilterWriter {

    public ArticleCSVWriter(Writer out) {
        super(out);
    }

    public void writeArticle(Article a) throws IOException {
        out.write(a.getId() + ", " + a.getName() + ", " + a.getPrice() + ";");
    }

}
