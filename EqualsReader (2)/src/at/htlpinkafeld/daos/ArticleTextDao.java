package at.htlpinkafeld.daos;

import at.htlpinkafeld.pojos.Article;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ArticleTextDao {

    private String filename;

    public ArticleTextDao(String fName) {
        this.filename = fName;
    }

    public void write(List<Article> articles) throws IOException {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter("output.csv"));
            for (Article a : articles) {
                out.print(a.getId() + ", " + a.getName() + ", " + a.getPrice() + ";");      //a.toString --> wenn es eine im Article Pojo gibt
            }
        } finally {
            out.close();
        }
    }

    public List<Article> read() throws IOException {
        List<Article> articleList = new ArrayList<>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filename));
            String line = in.readLine();
            String[] articles = line.split(";");
            for (String s : articles) {
                String[] splitLine = s.split(", ");
                int id = Integer.parseInt(splitLine[0]);
                double price = Double.parseDouble(splitLine[2]);
                articleList.add(new Article(id, splitLine[1], price));
            }
        } finally {
            in.close();
        }
        return articleList;
    }
}
