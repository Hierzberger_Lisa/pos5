package at.htlpinkafeld.main;

import at.htlpinkafeld.daos.ArticleTextDao;
import at.htlpinkafeld.pojos.Article;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) throws IOException {
        List<Article> articles = new ArrayList<>();
        articles.add(new Article(1, "Mouse", 49.99));
        articles.add(new Article(2, "Keyboard", 59.99));
        
        ArticleTextDao dao = new ArticleTextDao("input.csv");
        
        dao.write(articles);
        
        for(Article a : dao.read()){
            System.out.println(a.toString());
        }
    }
}
