/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.daos;

import at.htlpinkafeld.pojos.Article;
import java.io.IOException;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;

public class ArticleCSVReaderTest {
    
    public ArticleCSVReaderTest() {
    }

    @Test
    public void testRead() throws IOException {
        ArticleCSVReader a = new ArticleCSVReader(new StringReader("1, Keyboard, 39.99;"));
        Article article = a.readArticle();
        assertEquals(new Article(1, "Keyboard", 39.99), article);
    }
}
