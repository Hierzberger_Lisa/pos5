/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.daos;

import at.htlpinkafeld.pojos.Article;
import java.io.IOException;
import java.io.StringWriter;
import org.junit.Test;
import static org.junit.Assert.*;

public class ArticleCSVWriterTest {

    public ArticleCSVWriterTest() {
    }

    @Test
    public void testWrite() throws IOException {
        ArticleCSVWriter a = new ArticleCSVWriter(new StringWriter());
        a.writeArticle(new Article(1, "Sofa", 689.99));
        assertEquals("1, Sofa, 689.99;", a.toString());
    }
}
