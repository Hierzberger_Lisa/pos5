// @author eisin

package at.htlpinkafeld.readers;

import java.io.FilterReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;


public class EqualsReader extends FilterReader{

    PushbackReader reader;
    
    public EqualsReader(Reader in) {
        super(new PushbackReader(in, 3));
        this.reader = (PushbackReader) this.in;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int numRead = 0;
        for(int i = 0; i < off + len; i++){
            int c = this.read();
            if(c != -1){
                cbuf[i] = (char) c;
                numRead++;
            }else{
                break;
            }
        }
        if(numRead == 0){
            numRead = -1;
        }
        return numRead;
    }

    @Override
    public int read() throws IOException {
        int input = super.read();
        int input2;
        if(input != -1){
            if(input == '='){
                input2 = super.read();
                if(input2 == '='){
                    reader.unread(new char[]{'e', 'q', '.'});
                    return '.';
                }else{
                    if(input2 != -1){
                        reader.unread(input2);
                    }
                    reader.unread('-');
                    return '<';
                }
            }
        }
        return input;
    }
}
