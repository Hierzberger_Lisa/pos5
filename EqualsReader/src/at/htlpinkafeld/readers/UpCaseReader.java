// @author eisin

package at.htlpinkafeld.readers;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;


public class UpCaseReader extends FilterReader{

    public UpCaseReader(Reader in) {
        super(in);
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int numRead = 0;
        for(int i = 0; i < off + len; i++){
            int c = this.read();
            if(c != -1){
                cbuf[i] = (char) c;
                numRead++;
            }else{
                break;
            }
        }
        if(numRead == 0){
            numRead = -1;
        }
        return numRead;
    }

    @Override
    public int read() throws IOException {
        int input = super.read();
        if(input != -1){
            return Character.toUpperCase((char)input);
        }
        return -1;
    }
}