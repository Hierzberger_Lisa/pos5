/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author eisin
 */
public class EqualsReaderTest {

    public EqualsReaderTest() {
    }

    String line = "if (a == 4) a = 0;\n";
    String lineConv = "if (a .eq. 4) a <- 0;";

    @Test
    public void testSomeMethod() throws IOException {
        BufferedReader br = new BufferedReader(
                            new EqualsReader(
                            new StringReader(line)));
        assertEquals(lineConv, br.readLine());
    }

}
