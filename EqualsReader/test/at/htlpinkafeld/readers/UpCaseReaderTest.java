/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author eisin
 */
public class UpCaseReaderTest {
    
    public UpCaseReaderTest() {
    }
    
    String line = "Hello World!";
    String lineUpperCase = "HELLO WORLD!";

    @Test
    public void testSomeMethod() throws IOException {
        BufferedReader reader = new BufferedReader(
                                new UpCaseReader(
                                new StringReader(line)));
        assertEquals(lineUpperCase, reader.readLine());
    }
    
}
