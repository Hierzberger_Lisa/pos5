/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filterexample;

import java.lang.reflect.Array;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author Lisa
 */
public class FilterExample {

    public static void main(String[] args) {
        List<String> names = null;
        
        names.add("Peter");
        names.add("Sam");
        names.add("Greg");
        names.add("Ryan");

        for (String name : names) {
            if (!name.equals("Sam")) {
                System.out.println(name);
            }
        }

        names.stream()
                .filter(new Predicate<String>() {
                    public boolean test(String name) {
                        return !name.equals("Sam");
                    }
                })
                .forEach(name -> System.out.println(name));
    }

}
