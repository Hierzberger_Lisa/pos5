/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
//Decorator
public abstract class Beilagen implements Gericht { 
    protected Gericht gericht; 

    public Beilagen(Gericht gericht) { 
        this.gericht = gericht; 
    } 
    
    @Override 
    public double getPreis() {
        return gericht.getPreis();
    }
    
    @Override
    public void getBeschreibung() {
        gericht.getBeschreibung();
    }
} 