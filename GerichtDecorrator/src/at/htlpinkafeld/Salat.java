/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class Salat extends Beilagen {
    public Salat(Gericht g) {
        super(g);
    }

    @Override
    public double getPreis() {
        return gericht.getPreis() + 2.25;
    }

    @Override
    public void getBeschreibung() {
        gericht.getBeschreibung(); 
        System.out.print(" mit  Salat"); 
    }
}
