/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class client {

    public static void main(String[] args) {
        Gericht gericht = new Salat(new Nudeln(new Schnitzl()));
        gericht.getBeschreibung();
        //Schnitzl, Nudeln, Salat 
        System.out.println(" für " + gericht.getPreis() + " Euro");
        // für 19.75 Euro 

        gericht = new Suppe(gericht);
        gericht.getBeschreibung();
        //Schnitzl, Nudeln, Salat, Suppe 
        System.out.println(" für " + gericht.getPreis() + " Euro");
        // für 21.25 Euro 
    }

}
