/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageproxy;

/**
 *
 * @author Lisa
 */
//RealSubject
public class HighResolutionImage implements Image {

    public HighResolutionImage(String imageFilePath) {
        loadImage(imageFilePath);
    }

    private void loadImage(String imageFilePath) {
        // load Image from disk into memory
        // this is heavy and costly operation
    }

    @Override
    public void showImage() {
        // Actual Image rendering logic
    }

}
