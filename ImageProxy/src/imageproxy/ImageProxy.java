/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageproxy;

/**
 *
 * @author Lisa
 */
//Proxy
public class ImageProxy implements Image {
    /* Private Proxy data */
    private String imageFilePath;

    /**
     * Reference to RealSubject
     */
    private HighResolutionImage proxifiedImage;

    public ImageProxy(String imageFilePath) {
        this.imageFilePath = imageFilePath;
    }

    @Override
    public void showImage() {
        // create the Image Object only when the image is required to be shown
        if (proxifiedImage == null) {
            proxifiedImage = new HighResolutionImage(imageFilePath);
        }

        // now call showImage on realSubject
        proxifiedImage.showImage();

    }
}