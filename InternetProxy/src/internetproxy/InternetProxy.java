/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package internetproxy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lisa
 */
public class InternetProxy implements Internet {

    private Internet internet = new RealInternet();
    private static List<String> bannedSites;

    static {
        bannedSites = new ArrayList<String>();
        bannedSites.add("abc.com");
        bannedSites.add("def.com");
        bannedSites.add("ijk.com");
        bannedSites.add("lnm.com");
    }

    @Override
    public void connectTo(String host) {
        if (bannedSites.contains(host.toLowerCase())) {
            try {
                throw new Exception("Access Denied");
            } catch (Exception ex) {
                Logger.getLogger(InternetProxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        internet.connectTo(host);
    }

}
