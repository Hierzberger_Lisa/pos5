/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.pojo;

/**
 *
 * @author Lisa
 */
public class Display {

    private String dispString;   //String shown in the JList
    private Student source;

    public Display(String dispString, Student source) {
        this.dispString = dispString;
        this.source = source;
    }

    public String getDispString() {
        return dispString;
    }

    public void setDispString(String dispString) {
        this.dispString = dispString;
    }

    public Student getSource() {
        return source;
    }

    public void setSource(Student source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "Display{" + "dispString=" + dispString + ", source=" + source + '}';
    }

}
