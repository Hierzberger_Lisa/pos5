/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.pojo;

import java.time.LocalDate;

/**
 *
 * @author Lisa
 */
public class Student {
     private String name;
    private String university;
    private LocalDate birthday;
    private Integer registrationNumber;

    public Student(String name, String university, LocalDate birthday, Integer registrationNumber) {
        this.name = name;
        this.university = university;
        this.birthday = birthday;
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Integer getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Integer registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", university=" + university + ", birthday=" + birthday + ", registrationNumber=" + registrationNumber + '}';
    }

    
}
