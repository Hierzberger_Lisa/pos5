/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.presentation;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author Lisa
 */
public class MainFrame extends JFrame implements ActionListener{

    
    JButton action; 
    JLabel label;
    
    public MainFrame(String title) throws HeadlessException {
        super(title);

        this.setLocation(0, 0);
        this.setSize(900, 600);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(0, 1));

        //-----------------------------
        
        JPanel checkboxes = new JPanel();
        JCheckBox date = new JCheckBox("by Date");
        JCheckBox names = new JCheckBox("by Names");
        JCheckBox universities = new JCheckBox("by University");
        JCheckBox even = new JCheckBox("by even Reg.Number");
        JCheckBox odd = new JCheckBox("by odd Reg.Number");

        checkboxes.add(date);
        checkboxes.add(names);
        checkboxes.add(universities);
        checkboxes.add(even);
        checkboxes.add(odd);

        checkboxes.setBorder(BorderFactory.createTitledBorder("Select filters:"));
        this.add(checkboxes);
        
        
        //-----------------------------
        
        JPanel criterias = new JPanel();

        JTextField startswithTextfield = new JTextField(20);
        JTextField DateLimitTextfield = new JTextField(20);
        criterias.add(new JLabel("Name/Univ. starts with:"));
        criterias.add(startswithTextfield);
        criterias.add(new JLabel("Date limit:"));
        criterias.add(DateLimitTextfield);
        criterias.add(new JLabel("dd.mm.yyyy"));

        criterias.setBorder(BorderFactory.createTitledBorder("Enter criterias:"));
        this.add(criterias);
        //-----------------------------
        JPanel sortCriterias = new JPanel();

        JRadioButton radioDate = new JRadioButton("Sort by Date");
        JRadioButton radioName = new JRadioButton("Sort by Name");
        JRadioButton radioUniversity = new JRadioButton("Sort by University");
        JRadioButton radioRegNumber = new JRadioButton("Sort by Registrationnumber");

        sortCriterias.add(radioDate);
        sortCriterias.add(radioName);
        sortCriterias.add(radioUniversity);
        sortCriterias.add(radioRegNumber);

        sortCriterias.setBorder(BorderFactory.createTitledBorder("Select sort criteria:"));
        this.add(sortCriterias);
        //-----------------------------
        JPanel actions = new JPanel();

        JCheckBox addAge = new JCheckBox("Add Age");
        JCheckBox addName = new JCheckBox("Add Name");
        JCheckBox addUniversity = new JCheckBox("Add University");
        JCheckBox printAge = new JCheckBox("Print Age");
        JCheckBox printName = new JCheckBox("Print Name");
        JCheckBox printUniversity = new JCheckBox("Print University");

        actions.add(addAge);
        actions.add(addName);
        actions.add(addUniversity);
        actions.add(printAge);
        actions.add(printName);
        actions.add(printUniversity);

        actions.setBorder(BorderFactory.createTitledBorder("Select actions:"));
        this.add(actions);

        //-----------------------------
        action = new JButton("Action");
        this.add(action);
        action.addActionListener(this);


        //-----------------------------
        this.setVisible(true);
    }

    public static void main(String[] args) throws InterruptedException, InvocationTargetException {
        SwingUtilities.invokeLater(() -> { new MainFrame("FlexibleStudentList");
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == action) {
            System.out.println("Button geklickt!");
        }
    }
}
