/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.service;


import at.htlpinkafeld.pojo.Display;
import at.htlpinkafeld.pojo.Student;
import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
/**
 *
 * @author Lisa
 */
public class AppService<T> {

    private List<Student> studentList = new ArrayList<>();

    private AppService() {
        studentList.add(new Student("Rob", "New Jersey",
                LocalDate.of(1993, 3, 18), 7));
        studentList.add(new Student("Alf", "Aspen",
                LocalDate.of(1970, 3, 18), 1));
        studentList.add(new Student("Bert", "Berlin",
                LocalDate.of(1964, 3, 18), 2));
        studentList.add(new Student("Anton", "Vienna",
                LocalDate.of(1969, 3, 18), 3));
        studentList.add(new Student("Andi", "Augusta",
                LocalDate.of(1964, 3, 18), 4));
        studentList.add(new Student("Gerd", "Vienna",
                LocalDate.of(2000, 3, 18), 5));
        studentList.add(new Student("Sue", "Aspen",
                LocalDate.of(1996, 3, 18), 6));
    }

    private Function<T, String> mapper = t -> t.toString();
    private List<Predicate<? super T>> predicates = new LinkedList<>();
    private Comparator<? super T> comparator = (a, b) -> 0;

    public void addPredicate(Predicate<? super T> o) {
        predicates.add(o);
    }

    public void removePredicate(Predicate<? super T> o) {
        predicates.remove(o);
    }

    public void setComparator(Comparator<? super T> comp) {
        comparator = comp;
    }

    public void setMapper(Function<T, String> mapper) {
        this.mapper = mapper;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Display> processList() {
        Stream<Student> stream = studentList.stream();
        List<Display> processedList = new LinkedList<>();

        for (Predicate pred : predicates) {
            stream = stream.filter(pred);
        }

        return processedList;
    }

}
