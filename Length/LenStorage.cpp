#include "LenStorage.h"

LenStorage::LenStorage(int length): size(length){
    this->arr = new Length[length];
}

LenStorage::LenStorage(const LenStorage &lenStorage): size(lenStorage.size){
    this->arr = new Length[lenStorage.size];

    for(int cnt = 0; cnt < lenStorage.size; cnt++){
        this->arr[cnt] = lenStorage.arr[cnt];
    }
}

LenStorage::~LenStorage(){
    delete[] this->arr;
}

void LenStorage::set(int idx, const Length &len) const{
    if(this->indexOK(idx)){
        this->arr[idx] = len;
    }
}

Length LenStorage::get(int idx) const{
    if(this->indexOK(idx)){
        return this->arr[idx];
    }

    return NULL;
}

int LenStorage::getSize() const{
    return this->size;
}

void LenStorage::resize(int newSize){
    Length* newarr = new Length[newSize];

    for(int cnt = 0; cnt < newSize && cnt < this->size; cnt++){
        newarr[cnt] = this->arr[cnt];
    }

    delete[] this->arr;

    this->arr = newarr;
}

LenStorage& LenStorage::operator=(const LenStorage& lenStorage){
    if(this != &lenStorage){
        this->size = lenStorage.size;

        delete[] this->arr;
        this->arr = new Length[lenStorage.size];

        for(int cnt = 0; cnt < lenStorage.size; cnt++){
            this->arr[cnt] = lenStorage.arr[cnt];
        }
    }

    return (*this);
}

ostream& operator<<(ostream &os, const LenStorage &lMgr){
    os << "Size: " << lMgr.size << ", [";

    for(int cnt = 0; cnt < lMgr.size; cnt++){
        os << lMgr.arr[cnt];

        if(cnt < (lMgr.size - 1)){
            os << ",";
        }
    }

    os << "]" << endl;

    return os;
}

bool LenStorage::indexOK(int idx) const{
    return idx >= 0 && idx < this->size;
}
