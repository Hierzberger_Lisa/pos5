#pragma once
#include <iostream>
#include "Length.h"

using namespace std;

class LenStorage{
    private:
        Length *arr;
        int size;
        bool indexOK(int idx) const;
    public:
        LenStorage(int length=10);
        LenStorage(const LenStorage &lenStorage);
        ~LenStorage();
        void set(int idx, const Length &len) const;
        Length get(int idx) const;
        int getSize() const;
        void resize(int newSize);
        LenStorage& operator=(const LenStorage& lenStorage);
        friend ostream& operator<<(ostream &os, const LenStorage &lMgr);
};
