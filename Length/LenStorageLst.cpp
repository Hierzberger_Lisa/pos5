#include "LenStorageLst.h"

LenStorageLst::LenStorageLst(){
    this->arr = new Length[0];
    this->size = 0;
}

LenStorageLst::~LenStorageLst(){
    delete[] this->arr;
}

LenStorageLst::LenStorageLst(const LenStorageLst &list):size(list.size){
    this->arr = new Length[list.size];

    for(int cnt = 0; cnt < list.size; cnt++){
        this->arr[cnt] = list.arr[cnt];
    }
}

void LenStorageLst::set(int idx, const Length &len){
    if(this->indexOK(idx)){
        this->arr[idx] = len;
    }
}

Length LenStorageLst::get(int idx) const{
    if(this->indexOK(idx)){
        return this->arr[idx];
    }

    return NULL;
}

void LenStorageLst::add(const Length &len){
    this->add(this->size, len);
}

void LenStorageLst::add(int idx, const Length &len){
    this->size++;

    if(this->indexOK(idx)){
        Length *newarr = new Length[this->size];

        for(int cnt = 0; cnt < idx; cnt++){
            newarr[cnt] = this->arr[cnt];
        }

        newarr[idx] = len;

        for(int cnt = idx+1; cnt < this->size; cnt++){
            newarr[cnt] = this->arr[cnt-1];
        }

        delete[] this->arr;
        this->arr = newarr;
    }else{
        this->size--;
    }
}

Length LenStorageLst::remove(){
    return this->remove(this->size - 1);
}

Length LenStorageLst::remove(int idx){
    if(this->indexOK(idx)){
        Length* newarr = new Length[this->size-1];
        Length reLength;

        for(int cnt = 0; cnt < idx; cnt++){
            newarr[cnt] = this->arr[cnt];
        }

        reLength = newarr[idx];

        for(int cnt = idx + 1; cnt < this->size; cnt++){
            newarr[cnt-1] = this->arr[cnt];
        }

        delete[] this->arr;
        this->arr = newarr;
        this->size--;

        return reLength;
    }else{
        return NULL;
    }
}

int LenStorageLst::getSize() const{
    return this->size;
}

LenStorageLst& LenStorageLst::operator=(const LenStorageLst& list){
    if(this != &list){
        delete[] this->arr;

        this->size = list.size;
        this->arr = new Length[list.size];

        for(int cnt = 0; cnt < list.size; cnt++){
            this->arr[cnt] = list.arr[cnt];
        }
    }

    return *this;
}

ostream& operator<<(ostream &os, const LenStorageLst &list){
    os << "Size: " << list.size << ", [";

    for(int cnt = 0; cnt < list.size; cnt++){
        os << list.arr[cnt];

        if(cnt < (list.size - 1)){
            os << ",";
        }
    }

    os << "]" << endl;

    return os;
}

bool LenStorageLst::indexOK(int idx) const{
    return idx >= 0 && idx < this->size;
}
