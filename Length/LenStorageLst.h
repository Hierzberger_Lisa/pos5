#pragma once
#include "Length.h"
#include <iostream>

using namespace std;

class LenStorageLst{
    private:
        Length *arr;
        int size;
        bool indexOK(int idx) const;
    public:
        LenStorageLst();
        ~LenStorageLst();
        LenStorageLst(const LenStorageLst &list);
        void set(int idx, const Length &len);
        Length get(int idx) const;
        void add(const Length &len);
        void add(int idx, const Length &len);
        Length remove();
        Length remove(int idx);
        int getSize() const;
        LenStorageLst& operator=(const LenStorageLst& list);
        friend ostream& operator<<(ostream &os, const LenStorageLst &list);
};
