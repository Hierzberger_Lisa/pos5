#include <iostream>
#include "Length.h"
#include "LenStorage.h"

using namespace std;

int main(){

    LenStorage lenSt(2);
    LenStorage *lenStP;

    lenSt.set(0, Length(3));
    lenSt.set(1, Length(4, LENUNIT_MM));

    //copy constructor wird verwendet
    lenStP = new LenStorage(lenSt);
    lenStP->set(1, Length(40, LENUNIT_INCH));

    cout << "lenSt: " << endl;
    cout << lenSt << endl;

    cout << "lenStP: " << endl;
    cout << *lenStP << endl;

    //the destructor frees the dynamic memory of *lenStP
    delete lenStP;

    lenSt.set(1, Length(4, LENUNIT_MM));
    cout << "lenSt: " << endl;
    cout << lenSt << endl;

    return 0;
}
