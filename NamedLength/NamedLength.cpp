#include "NamedLength.h"

NamedLength::NamedLength(const string &lbl, double v, LengthUnit u):Length(v, u), label(lbl){}

NamedLength::NamedLength(const string &lbl, const Length &len):Length(len), label(lbl){}

NamedLength::~NamedLength(){ }

string NamedLength::toString() const{
    stringstream sTr;

    sTr << (*this) << this->Length::toString();

    return sTr.str();
}

ostream& operator<<(ostream& os, const NamedLength& namedLength){
    os << namedLength.label << ": ";
    return os;
}
