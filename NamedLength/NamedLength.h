#pragma once
#include "Length.h"
#include <string>
#include <iostream>

using namespace std;

class NamedLength: public Length{
    private:
        string label;
    public:
        NamedLength(const string &lbl="N.N.", double v=0, LengthUnit u=LENUNIT_M );
        NamedLength(const string &lbl, const Length &len);
        virtual ~NamedLength();
        virtual string toString() const;
        friend ostream& operator<<(ostream& os, const NamedLength& namedLength);
};
