#include <iostream>
#include "Length.h"
#include "NamedLength.h"

using namespace std;

int main(){

    Length len1;
    Length len2(2, LENUNIT_MM);

    NamedLength nlen1("LongJumpWorldRecord", 8.95);
    NamedLength nlen2("WorldsBiggestTV", 370, LENUNIT_INCH);

    Length lenArr[] = {len1, len2, nlen1, nlen2};
    Length *lenPtrArr[] = {&len1, &len2, &nlen1, &nlen2};

    cout << endl << "Length Collection:" << endl;

    for(int i=0; i<4; i++)
        cout << lenArr[i].toString() << endl;

    cout << endl << "Length Pointer Collection:" << endl;

    for(int i=0; i<4; i++)
        cout << lenPtrArr[i]->toString() << endl;

    return 0;
}
