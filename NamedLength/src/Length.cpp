#include "Length.h"

string Length::units[] = {string("m"), string("mm"), string("inch")};

double Length::convFact[] = {1.0, 0.001, 0.0254};

Length::Length(double value, LengthUnit unit){
    this->value = value;
    this->unit = unit;
}

Length::~Length(){ }

void Length::show() const {
    cout << this->value << unitToString(this->unit);
}

string Length::toString() const{
    stringstream sTr;

    sTr << (*this);

    return sTr.str();
}

Length& Length::operator+=(const Length& len){
    double length1 = Length::unitToMeter(this->value, this->unit);
    double length2 = Length::unitToMeter(len.value, len.unit);
    double sum = length1 + length2;

    this->value = Length::meterToUnit(sum, this->unit);

    return *this;
}

Length& Length::operator++(){
    this->value++;
    return *this;
}

Length Length::operator++(int dummy){
    Length cop(*this);

    this->value++;

    return cop;
}

Length& Length::operator--(){
    this->value--;
    return *this;
}

Length Length::operator--(int dummy){
    Length cop(*this);

    this->value--;

    return cop;
}

Length operator+(const Length &len, const Length &len1){
    double length = Length::unitToMeter(len.value, len.unit);
    double length2 = Length::unitToMeter(len1.value, len1.unit);

    return Length(Length::meterToUnit((length + length2), len.unit), len.unit);
}

ostream& operator<<(ostream &os, const Length &length){
    os << length.value << Length::unitToString(length.unit);
    return os;
}

bool operator==(const Length &len, const Length &len1){
    return compareValue(Length::unitToMeter(len.value, len.unit), Length::unitToMeter(len1.value, len1.unit));
}

bool operator!=(const Length &len, const Length &len1){
    return !compareValue(Length::unitToMeter(len.value, len.unit), Length::unitToMeter(len1.value, len1.unit));
}

bool operator<(const Length &len, const Length &len1){
    return Length::unitToMeter(len.value, len.unit) < Length::unitToMeter(len1.value, len1.unit);
}

bool operator<=(const Length &len, const Length &len1){
    return Length::unitToMeter(len.value, len.unit) <= Length::unitToMeter(len1.value, len1.unit);
}

bool operator>(const Length &len, const Length &len1){
    return Length::unitToMeter(len.value, len.unit) > Length::unitToMeter(len1.value, len1.unit);
}

bool operator>=(const Length &len, const Length &len1){
    return Length::unitToMeter(len.value, len.unit) >= Length::unitToMeter(len1.value, len1.unit);
}

string Length::unitToString(LengthUnit u){
    return units[u];
}

double Length::unitToMeter(double val, LengthUnit u){
    return (val * convFact[u]);
}

double Length::meterToUnit(double val, LengthUnit u){
    return (val / convFact[u]);
}

bool compareValue(double val, double val2){
    return val == val2;
}
