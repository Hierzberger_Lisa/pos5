/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */

//ConcreteComponent
public class BasePizza implements Pizza{

    @Override
    public String bakePizza() {
        return "Base Pizza";
    }

    @Override
    public float getCost() {
        return 100;
    }
    
}
