package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */

//Main
public class Client {

    public static void main(String[] args) {
        String s;
        float c;
        Pizza p1 = new Mashrooms(new BasePizza());
        s = p1.bakePizza();
        c = p1.getCost();
        System.out.println(s+ " costs €" +  c);
        
        
        Pizza p2 = new Pepperoni(new Mashrooms(new BasePizza()));
        s = p2.bakePizza();
        c = p2.getCost();
        System.out.println(s + " costs €" +  c);
    }
    
}
/*        Car c = new LuxuryCar(new BasicCar());
        c.assemble();
        System.out.println("\n*****");

        Car c2 = new SportsCar(new BasicCar());
        c2.assemble();*/