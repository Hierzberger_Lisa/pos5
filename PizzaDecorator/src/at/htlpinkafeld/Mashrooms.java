/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class Mashrooms extends PizzaDecorator {

    public Mashrooms(Pizza p) {
        super(p);
    }

    @Override
    public String bakePizza() {
        return p.bakePizza() + " with Mashrooms";
    }

    @Override
    public float getCost() {
        return p.getCost() + 30;
    }
}
