/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */

//Decorator
public class PizzaDecorator implements Pizza {
    Pizza p;
    
    public PizzaDecorator (Pizza p) {
        this.p = p;
    }
    
    @Override
    public String bakePizza() {
        return p.bakePizza();
    }
    
    @Override 
    public float getCost() {
        return p.getCost();
    }
}
