#pragma once
#include <string>
#include <iostream>
#include <sstream>
#include "Competitor.h"
#include "Date.h"

using namespace std;

class Competition{

    private:
        string kindOfSport;
        string place;
        Date date;
        Competitor** participants;
        static const int SIZE = 10;

        int paticipantsSize;
        int currentParticipants;

        void resize(int newsize);


    public:
        Competition(const string& kindOfSport, const string& place, const Date& date);

        void add(Competitor* com);
        string showResult() const;

        friend ostream& operator<<(ostream& os, const Competition &competition);
 };
