#pragma once
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class Competitor{

    private:
        string name;
        int bib;

    public:
        Competitor(const string& name, int bib);
        virtual ~Competitor();

        void setName(const string& name);
        void setBib(int bib);

        string getName() const;
        int getBib() const;

        virtual bool isBetterThan(const Competitor& c) const = 0;
        virtual string toResult() const;

        friend ostream& operator<<(ostream& os, const Competitor& competitor);
};
