#pragma once
#include <iostream>

using namespace std;

class Date{

    private:
        int day;
        int month;
        int year;

    public:
        Date(int day = 1, int month = 1, int year = 1900);

        int getDay() const;
        int getMonth() const;
        int getYear() const;

        void setDay(int day);
        void setMonth(int month);
        void setYear(int year);

        friend ostream& operator<<(ostream& os, const Date& date);
};
