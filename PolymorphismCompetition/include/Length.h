#pragma once
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

enum LengthUnit {LENUNIT_M, LENUNIT_MM, LENUNIT_INCH};

class Length{

    private:
        static string units[];
        static double convFact[];
        double value;
        LengthUnit unit;

    public:
        Length(double value = 0, LengthUnit u = LENUNIT_M);
        virtual ~Length();

        void show() const;
        virtual string toString() const;

        Length& operator++();
        Length operator++(int dummy);

        Length& operator--();
        Length operator--(int dummy);

        friend Length operator+(const Length &len, const Length& len1);
        Length& operator+=(const Length& len);

        friend ostream& operator<<(ostream &os, const Length& length);

        friend bool operator==(const Length &len, const Length& len1);
        friend bool operator!=(const Length &len, const Length& len1);

        friend bool operator<(const Length &len, const Length& len1);
        friend bool operator<=(const Length &len, const Length& len1);
        friend bool operator>(const Length &len, const Length& len1);
        friend bool operator>=(const Length &len, const Length& len1);

        static string unitToString(LengthUnit u);
        static double unitToMeter(double val, LengthUnit u);
        static double meterToUnit(double val, LengthUnit u);
};

bool compareValue(double val, double val2);
