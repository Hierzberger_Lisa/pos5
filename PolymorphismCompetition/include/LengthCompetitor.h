#pragma once
#include "Competitor.h"
#include "Length.h"

class LengthCompetitor: public Competitor{

    private:
        Length len;

    public:
        LengthCompetitor(const string& name, int bib, const Length& length);
        LengthCompetitor(const string& name, int bib, int length, LengthUnit unit = LENUNIT_M);
        virtual ~LengthCompetitor();

        Length getLength() const;

        virtual bool isBetterThan(const Competitor& c) const;

        virtual string toResult() const;

        friend ostream& operator<<(ostream& os, const LengthCompetitor& competitor);
};
