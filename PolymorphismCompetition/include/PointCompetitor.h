#pragma once
#include "Competitor.h"

class PointCompetitor: public Competitor{

    private:
        int point;

    public:
        PointCompetitor(const string& name, int bib, int point);
        virtual ~PointCompetitor();

        int getPoint() const;

        bool isBetterThan(const Competitor& c) const;
        virtual string toResult() const;

        friend ostream& operator<<(ostream& os, const PointCompetitor& competitor);
};
