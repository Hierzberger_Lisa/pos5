#pragma once
#include <iostream>

using namespace std;

enum Unit {SEC, MIN, HOUR};

class Time{

    private:
        double seconds;
        int minutes;
        int hours;

    public:
        Time(double seconds = 0.0, int minutes = 0, int hours = 0);

        void setSeconds(double seconds);
        double getSeconds() const;

        void setMinutes(int minutes);
        int getMinutes() const;

        void setHours(int hours);
        int getHours() const;

        void setTime(double val, Unit u = SEC);
        double getTime(Unit u = SEC);

        void validate();
        void show() const;

        Time& operator++();
        Time& operator--();
        Time operator++(int dummy);
        Time operator--(int dummy);

        Time& operator+=(const Time& time);
        Time& operator-=(const Time& time);

        friend bool operator<(const Time &time, const Time &time1);
        friend bool operator>(const Time &time, const Time &time1);

        friend bool operator<=(const Time &time, const Time &time1);
        friend bool operator>=(const Time &time, const Time &time1);

        friend Time operator+(const Time &time, const Time &time1);
        friend Time operator-(const Time &time, const Time &time1);

        friend ostream& operator<<(ostream& os, const Time& time);
};
