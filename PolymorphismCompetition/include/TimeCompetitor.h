#pragma once
#include "Competitor.h"
#include "Time.h"

class TimeCompetitor: public Competitor{

    private:
        Time time;

    public:
        TimeCompetitor(const string& name, int bib, const Time& time);
        TimeCompetitor(const string& name, int bib, double seconds, int minutes, int hours);
        virtual ~TimeCompetitor();

        Time getTime() const;

        virtual bool isBetterThan(const Competitor& c) const;
        virtual string toResult() const;

        friend ostream& operator<<(ostream& os, const TimeCompetitor& competitor);
};
