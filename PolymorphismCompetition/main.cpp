#include <iostream>
#include "Competition.h"
#include "PointCompetitor.h"

using namespace std;

int main() {

    Competition c("Ski Jumping - Men's Large Hill;", "Bischofshofen - Austria", Date(06,01,2020));

    PointCompetitor pk1("KOBAYASHI R.", 47, 282);
    PointCompetitor pk2("KUBACKI D.", 45, 268);
    PointCompetitor pk3("KRAFT S. ", 50, 268);

    Competitor* comp1 = &pk1;
    Competitor* comp2 = &pk2;
    Competitor* comp3 = &pk3;

    c.add(comp1);
    c.add(comp2);
    c.add(comp3);

    cout << (c.showResult());

    return 0;
}
