#include "Competition.h"

Competition::Competition(const string& kindOfSport, const string& place, const Date& date): kindOfSport(kindOfSport), place(place), date(date), paticipantsSize(SIZE), currentParticipants(0){
    this->participants = new Competitor*[this->paticipantsSize];
}

void Competition::add(Competitor* com){
    if(this->currentParticipants >= this->paticipantsSize){
        this->paticipantsSize += SIZE;
        this->resize(this->paticipantsSize);
    }

    int cnt = this->currentParticipants;

    for(; cnt > 0; cnt--){
        if(this->participants[cnt-1]->isBetterThan(*com)){
            break;
        }else{
            this->participants[cnt] = this->participants[cnt-1];
        }
    }

    this->participants[cnt] = com;
    this->currentParticipants++;
}

string Competition::showResult() const{
    stringstream sos;

    sos << (*this);

    return sos.str();
}

ostream& operator<<(ostream& os, const Competition &competition){
    os << competition.kindOfSport << "; " << competition.place << "; " << competition.date << endl;

    os << "Rank" << "\t" << "Bib" << "\t" << "Name" << "\t\t\t" << "Total" << endl;

    for(int cnt = 0; cnt < competition.currentParticipants; cnt++){
        os << (cnt+1) << "\t" << competition.participants[cnt]->toResult() << endl;
    }
    return os;
}

void Competition::resize(int newsize){
    Competitor** pointer = new Competitor*[newsize];

    for(int cnt = 0; cnt < newsize && cnt < this->currentParticipants; cnt++){
        pointer[cnt] = this->participants[cnt];
    }

    delete[] this->participants;
    this->participants = pointer;
}
