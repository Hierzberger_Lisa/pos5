#include "Competitor.h"

Competitor::Competitor(const string& name, int bib):name(name), bib(bib){}

Competitor::~Competitor(){}

void Competitor::setName(const string& name){
    this->name = name;
}

void Competitor::setBib(int bib){
    this->bib = bib;
}

string Competitor::getName() const{
    return this->name;
}

int Competitor::getBib() const{
    return this->bib;
}

string Competitor::toResult() const{
    stringstream sos;

    sos << (*this);

    return sos.str();
}

ostream& operator<<(ostream& os, const Competitor& competitor){
    os << competitor.bib << "\t" << competitor.name;

    return os;
}
