#include "Date.h"

Date::Date(int day, int month, int year):day(day), month(month), year(year){}

int Date::getDay() const{
    return this->day;
}

int Date::getMonth() const{
    return this->month;
}

int Date::getYear() const{
    return this->year;
}

void Date::setDay(int day){
    this->day = day;
}

void Date::setMonth(int month){
    this->month = month;
}

void Date::setYear(int year){
    this->year = year;
}

ostream& operator<<(ostream& os, const Date& date){
    os << date.day << "." << date.month << "." << date.year;

    return os;
}
