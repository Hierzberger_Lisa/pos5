#include "LengthCompetitor.h"

LengthCompetitor::LengthCompetitor(const string& name, int bib, const Length& length): Competitor(name, bib), len(length){}

LengthCompetitor::LengthCompetitor(const string& name, int bib, int length, LengthUnit unit): Competitor(name, bib), len(length, unit){}

LengthCompetitor::~LengthCompetitor(){}

Length LengthCompetitor::getLength() const{
    return this->len;
}

bool LengthCompetitor::isBetterThan(const Competitor& c) const{
    LengthCompetitor* com = (LengthCompetitor*) &c;

    return this->len > com->len;
}

string LengthCompetitor::toResult() const{
    stringstream sos;

    sos << Competitor::toResult() << "\t\t" << *this;

    return sos.str();
}

ostream& operator<<(ostream& os, const LengthCompetitor& competitor){
    os << competitor.len;

    return os;
}
