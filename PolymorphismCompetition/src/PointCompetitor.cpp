#include "PointCompetitor.h"

PointCompetitor::PointCompetitor(const string& name, int bib, int point): Competitor(name, bib), point(point){}

PointCompetitor::~PointCompetitor(){}

int PointCompetitor::getPoint() const{
    return this->point;
}

bool PointCompetitor::isBetterThan(const Competitor& c) const{
    PointCompetitor* com = (PointCompetitor*) &c;

    return this->point > com->point;
}

string PointCompetitor::toResult() const{
    stringstream s;
    s << Competitor::toResult() << "\t\t" << *this;

    return s.str();
}

ostream& operator<<(ostream& os, const PointCompetitor& competitor){
    os << competitor.point;

    return os;
}
