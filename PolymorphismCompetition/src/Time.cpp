#include "Time.h"
#include <math.h>

Time::Time(double seconds, int minutes, int hours){
    this->seconds = seconds;
    this->minutes = minutes;
    this->hours = hours;
    this->validate();
}

void Time::setSeconds(double seconds){
    this->seconds = seconds;
}

double Time::getSeconds() const{
    return this->seconds;
}

void Time::setMinutes(int minutes){
    this->minutes = minutes;
}

int Time::getMinutes() const{
    return this->minutes;
}

void Time::setHours(int hours){
    this->hours = hours;
}

int Time::getHours() const{
    return this->hours;
}

void Time::validate(){
    int minstoadd;
    int hourstoadd;

    if(this->seconds >= 0){
        minstoadd = (this->seconds / 60);
        this->seconds = fmod(this->seconds, 60.0);

    }else{
        minstoadd = (this->seconds > -60 ? -1 : 0) + (this->seconds / 60);
        this->seconds = 60.0 + fmod(this->seconds, 60.0);
    }

    this->minutes += minstoadd;

    if(this->minutes >= 0){
        hourstoadd = (this->minutes / 60);
        this->minutes = (this->minutes % 60);

    }else{
        hourstoadd = (this->minutes > -60 ? -1 : 0) + (this->minutes / 60);
        this->minutes = 60 + (this->minutes % 60);
    }

    this->hours += hourstoadd;
}

void Time::setTime(double val, Unit u){
    double seconds;

    switch(u){
        case SEC: seconds = val; break;
        case MIN: seconds = (val*60); break;
        case HOUR: seconds = (val*3600); break;
    }

    this->seconds = seconds;
    this->minutes = 0;
    this->hours = 0;
    this->validate();
}

double Time::getTime(Unit u){
    switch(u){
        case SEC: return this->seconds;
        case MIN: return this->minutes;
        case HOUR: return this->hours;
    }
    return 0.0;
}

void Time::show() const{
    cout << this->hours << ":" << this->minutes << ":" << this->seconds << endl;
}

Time& Time::operator++(){
    this->seconds += 1.0;
    this->validate();

    return (*this);
}

Time& Time::operator--(){
    this->seconds -= 1.0;
    this->validate();

    return (*this);
}

Time Time::operator++(int dummy){
    Time timecop(*this);

    this->seconds += 1.0;
    this->validate();

    return timecop;
}

Time Time::operator--(int dummy){
    Time timecop(*this);

    this->seconds -= 1.0;
    this->validate();

    return timecop;
}

Time& Time::operator+=(const Time& time){
    this->seconds += time.seconds;
    this->minutes += time.minutes;
    this->hours += time.hours;

    this->validate();

    return (*this);
}

Time& Time::operator-=(const Time& time){
    this->seconds -= time.seconds;
    this->minutes -= time.minutes;
    this->hours -= time.hours;

    this->validate();

    return (*this);
}

bool operator<(const Time &time, const Time &time1){
    double timesum = ((time.hours*3600) + (time.minutes*60) + time.seconds);
    double time1sum = ((time1.hours*3600) + (time1.minutes*60) + time1.seconds);

    return timesum < time1sum;
}

bool operator>(const Time &time, const Time &time1){
    double timesum = ((time.hours*3600) + (time.minutes*60) + time.seconds);
    double time1sum = ((time1.hours*3600) + (time1.minutes*60) + time1.seconds);

    return timesum > time1sum;
}

bool operator<=(const Time &time, const Time &time1){
    double timesum = ((time.hours*3600) + (time.minutes*60) + time.seconds);
    double time1sum = ((time1.hours*3600) + (time1.minutes*60) + time1.seconds);

    return timesum <= time1sum;
}

bool operator>=(const Time &time, const Time &time1){
    double timesum = ((time.hours*3600) + (time.minutes*60) + time.seconds);
    double time1sum = ((time1.hours*3600) + (time1.minutes*60) + time1.seconds);

    return timesum >= time1sum;
}

Time operator+(const Time &time, const Time &time1){
    int hours = time.hours + time1.hours;
    int minutes = time.minutes + time1.minutes;
    int seconds = time.seconds + time1.seconds;
    Time newTime(seconds, minutes, hours);

    newTime.validate();

    return newTime;
}

Time operator-(const Time &time, const Time &time1){
    int hours = time.hours - time1.hours;
    int minutes = time.minutes - time1.minutes;
    int seconds = time.seconds - time1.seconds;

    Time newTime(seconds, minutes, hours);

    newTime.validate();

    return newTime;
}

ostream& operator<<(ostream& os, const Time& time){
    os << time.hours << ":" << time.minutes << ":" << time.seconds << endl;

    return os;
}
