#include "TimeCompetitor.h"

TimeCompetitor::TimeCompetitor(const string& name, int bib, const Time& time):Competitor(name, bib), time(time){}

TimeCompetitor::TimeCompetitor(const string& name, int bib, double seconds, int minutes, int hours): Competitor(name, bib), time(seconds, minutes, hours){}

TimeCompetitor::~TimeCompetitor(){}

Time TimeCompetitor::getTime() const{
    return this->time;
}

bool TimeCompetitor::isBetterThan(const Competitor& c) const{
    TimeCompetitor* com = (TimeCompetitor*) &c;

    return this->time > com->time;
}

string TimeCompetitor::toResult() const{
    stringstream s;
    s << Competitor::toResult() << "\t\t" << *this;

    return s.str();
}

ostream& operator<<(ostream& os, const TimeCompetitor& competitor){
    os << competitor.time;
    return os;
}
