/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.proxycommandexecutor;

/**
 *
 * @author Sophie
 */

public class CheckInt implements Command {

    @Override
    public void run(String[] args) {
        for (String s : args) {
            if (!s.matches("\\d+")) {
                throw new NumberFormatException();
            }
        }
    }
}
