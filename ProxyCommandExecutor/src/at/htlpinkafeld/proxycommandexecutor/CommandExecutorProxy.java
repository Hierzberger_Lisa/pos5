/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.proxycommandexecutor;

import java.util.List;

/**
 *
 * @author Sophie
 */
public class CommandExecutorProxy implements CommandExecutor{
    private CommandExecutor commandexecutor;
    private String user;
    private List commandlist;

    
    public CommandExecutorProxy(String user, CommandExecutor executor){
        this.user=user;
        this.commandexecutor=executor;
    }
    
    @Override
    public void runCommand(String cmd, String[] args) {
        if(commandlist.contains(user)){
             this.commandexecutor.runCommand(cmd, args);
        }
       
        throw new NonAuthorizedException("User has no permission");
    }
}
