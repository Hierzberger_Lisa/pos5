/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.proxycommandexecutor;

/**
 *
 * @author Sophie
 */
public class NonAuthorizedException extends RuntimeException {

    public NonAuthorizedException(String s) {
        super(s);
    }
    
    public NonAuthorizedException(){
        super("No permissions");
    }
    
}
