/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.proxycommandexecutor;

/**
 *
 * @author Sophie
 */
public class PrintParams implements Command{

    @Override
    public void run(String[] args) {
        int i=1;
        for(String s: args){
            System.out.println("param"+i+":"+ s);
            i++;
        }
    }
    
}
