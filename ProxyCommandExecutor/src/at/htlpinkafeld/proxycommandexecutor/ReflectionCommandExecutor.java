/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.proxycommandexecutor;

/**
 *
 * @author Sophie
 */
public class ReflectionCommandExecutor implements CommandExecutor {

    @Override
    public void runCommand(String cmd, String[] args) {
        try {
            Class<Command> cmdClass= (Class<Command>) Class.forName("at.htlpinkafeld.proxy.cmd.PrintParams");
            Command commandObj = cmdClass.newInstance();
            commandObj.run(args);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {

        }
    }
}
