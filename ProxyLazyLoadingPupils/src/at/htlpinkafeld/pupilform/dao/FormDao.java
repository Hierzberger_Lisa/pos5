/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.pupilform.dao;

import at.htlpinkafeld.pupilform.pojo.Form;
import java.util.List;

/**
 *
 * @author Lisa
 */
public interface FormDao  {
    public List<Form> readAll();
}
