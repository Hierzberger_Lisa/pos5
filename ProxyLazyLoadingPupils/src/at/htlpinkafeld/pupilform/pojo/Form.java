/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.pupilform.pojo;

import java.util.List;

/**
 *
 * @author Lisa
 */
public interface Form {
    public String getName();
    public int getId();
    public void setId(int id);
    public void setName(String n);
}
