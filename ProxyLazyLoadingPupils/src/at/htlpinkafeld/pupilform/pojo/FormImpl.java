/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.pupilform.pojo;

import java.util.List;

/**
 *
 * @author Lisa
 */
public class FormImpl implements Form{

    private String name;
    private int id;
    private List<Pupil> pupilList;

    public FormImpl(String name, int id, List<Pupil> pupilList) {
        this.name = name;
        this.id = id;
        this.pupilList = pupilList;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
       this.id=id;
    }

    @Override
    public void setName(String n) {
        this.name =n;
    }

    public List<Pupil> getPupilList() {
        return pupilList;
    }

    public void setPupilList(List<Pupil> pupilList) {
        this.pupilList = pupilList;
    }
    

    
}
