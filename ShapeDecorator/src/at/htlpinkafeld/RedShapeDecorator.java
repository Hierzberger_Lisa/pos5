/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class RedShapeDecorator extends ShapeDecorator{
    
    public RedShapeDecorator(Shape s) {
        super(s);
    }
    
    @Override
    public void draw() {
        s.draw();
        setRedBorder(s);
    }
    
    private void setRedBorder(Shape s) {
        System.out.println("Border Color: RED");
    }
}
