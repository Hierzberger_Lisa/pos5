/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld;

/**
 *
 * @author Lisa
 */
public class ShapeDecorator implements Shape{
    public Shape s;

    public ShapeDecorator(Shape s) {
        this.s = s;
    }
    
    @Override
    public void draw() {
        s.draw();
    }
}
