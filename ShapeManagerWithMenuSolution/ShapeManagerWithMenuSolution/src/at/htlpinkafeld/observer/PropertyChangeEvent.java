/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.observer;

/**
 *
 * @author maue
 */
public class PropertyChangeEvent {
   private final Object source;
   private final String propertyName;
   private final Object newValue;
   private final Object oldValue;

   public PropertyChangeEvent(Object source, String propertyName, Object newValue, Object oldValue) {
      this.source = source;
      this.propertyName = propertyName;
      this.newValue = newValue;
      this.oldValue = oldValue;
   }

   public Object getSource() {
      return source;
   }

   public String getPropertyName() {
      return propertyName;
   }

   public Object getNewValue() {
      return newValue;
   }

   public Object getOldValue() {
      return oldValue;
   }
}
