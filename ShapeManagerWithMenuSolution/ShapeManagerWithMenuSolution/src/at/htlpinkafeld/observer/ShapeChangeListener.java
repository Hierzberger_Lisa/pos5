/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.observer;

import javax.swing.JFrame;

/**
 *
 * @author maue
 */
public class ShapeChangeListener implements PropertyChangeListener {
   JFrame frame;

   public ShapeChangeListener(JFrame frame) {
      this.frame = frame;
   }

   public ShapeChangeListener() {
      this(null);
   }
   
   public void setFrame(JFrame frame) {
      this.frame = frame;
   }

   @Override
   public void propertyChange(PropertyChangeEvent evt) {
      if( frame != null )
         frame.repaint();
   }
}
