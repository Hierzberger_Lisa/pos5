/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.domain;

import at.htlpinkafeld.observer.PropertyChangeEvent;
import at.htlpinkafeld.observer.PropertyChangeListener;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author maue
 */
public abstract class GeometricShape {

    public static final String PROP_COLOR = "COLOR";
    public static final String PROP_FILLED = "FILLED";

    private Color col;
    private boolean filled;

    private final List<PropertyChangeListener> pclList = new LinkedList<>();

    public GeometricShape(Color col, boolean filled) {
        setCol(col);
        setFilled(filled);
    }

    public Color getCol() {
        return col;
    }

    public void setCol(Color col) {
        Color oldCol = this.col;
        this.col = col;
        this.firePropertyChange(PROP_COLOR, oldCol, this.col);
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        boolean oldFilled = this.filled;
        this.filled = filled;
        this.firePropertyChange(PROP_FILLED, oldFilled, this.filled);
    }

    public abstract void draw(Graphics g);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pclList.add(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pclList.remove(listener);
    }

    public void firePropertyChange(String propertyName,
            Object oldValue, Object newValue) {
        if (oldValue == null || newValue == null
                || !oldValue.equals(newValue)) {
            for (PropertyChangeListener pcl : pclList) {
                pcl.propertyChange(new PropertyChangeEvent(this, propertyName,
                        oldValue, newValue));
            }
        }
    }

    public abstract Point getRefPoint();

    public abstract boolean isInside(Point pnt);

    public abstract void move(int byX, int byY);
}
