/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.domain;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author maue
 */
public class Rectangle extends GeometricShape {

    //Propertienames for PropertyChangeListener
    public static final String PROP_UPLEFT = "UPLEFT";
    public static final String PROP_WIDTH = "WIDTH";
    public static final String PROP_HEIGHT = "HIGHT";

    private Point upLeftPnt;
    private int width;
    private int height;
    
    Logger logger = LogManager.getLogger();

        
    public Rectangle(int ulX, int ulY, int width, int heigth, Color col, boolean filled) {
        super(col, filled);
        this.upLeftPnt = new Point(ulX, ulY);
        this.width = width;
        this.height = heigth;
    }

    public Point getUpLeftPnt() {
        return upLeftPnt;
    }

    public void setUpLeftPnt(Point upLeftPnt) {
        Point oldPnt = new Point(this.upLeftPnt);
        this.upLeftPnt = upLeftPnt;
        firePropertyChange(PROP_UPLEFT, oldPnt, this.upLeftPnt);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        int oldWidth = this.width;
        this.width = width;
        firePropertyChange(PROP_WIDTH, oldWidth, this.width);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        int oldHeight = this.height;
        this.height = height;
        firePropertyChange(PROP_HEIGHT, oldHeight, this.height);
    }

    @Override
    public void draw(Graphics g) {
        
        g.setColor(this.getCol());
        if (this.isFilled()) {
            g.fillRect(upLeftPnt.x, upLeftPnt.y, width, height);
            logger.info("Filled rectangle created");
        } else {
            g.drawRect(upLeftPnt.x, upLeftPnt.y, width, height);
            logger.info("Unfilled rectangle created");
        }
    }

    @Override
    public Point getRefPoint() {
        return this.upLeftPnt;
    }

    public void setLoRightPnt(Point point) {
        int w = point.x - upLeftPnt.x;
        int h = point.y - upLeftPnt.y;
        if (w < 0) {
            w = -w;
            upLeftPnt.x = point.x;
        }
        if (h < 0) {
            h = -h;
            upLeftPnt.y = point.y;
        }
        setWidth(w);
        setHeight(h);
    }

    @Override
    public boolean isInside(Point pnt) {
        boolean retVal;
        int dx, dy;
        dx = pnt.x - this.upLeftPnt.x;
        dy = pnt.y - this.upLeftPnt.y;

        retVal = (dx >= 0 && dx <= this.width
                && dy >= 0 && dy <= this.height);

        return retVal;
    }

    @Override
    public void move(int byX, int byY) {
        setUpLeftPnt(new Point(upLeftPnt.x + byX, upLeftPnt.y + byY));
    }
}
