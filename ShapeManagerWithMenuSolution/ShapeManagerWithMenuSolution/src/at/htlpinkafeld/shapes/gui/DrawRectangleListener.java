package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.shapes.domain.Rectangle;
import at.htlpinkafeld.shapes.domain.ShapeManager;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;

public class DrawRectangleListener extends MouseAdapter {

    private final Rectangle rect;
    private Boolean firstClick = true;
    private final ShapeManager s;

    DrawRectangleListener(ShapeManager sMgr, Color c, Boolean filled) {
        this.s = sMgr;
        rect = new Rectangle(0, 0, 0, 0, c, filled);
        s.addShape(rect);

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        if (firstClick) {
            rect.setUpLeftPnt(e.getPoint());
            firstClick = false;
        } else {
            rect.setLoRightPnt(e.getPoint());
            e.getComponent().removeMouseListener(this);
        }

    }

}
