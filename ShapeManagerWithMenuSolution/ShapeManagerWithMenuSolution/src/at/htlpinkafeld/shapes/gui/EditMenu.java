package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.utils.MenuUtils;
import static at.htlpinkafeld.utils.MenuUtils.createMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

class EditMenu extends JMenu {

    private final ShapeFrame outer;

    public EditMenu(final ShapeFrame outer) {
        super("Edit");
        this.outer = outer;
        JMenuItem create = new CreateMenu();
        add(create);
        add(new JSeparator());
        add(MenuUtils.createMenuItem("Move", "M", "M", null));
        add(MenuUtils.createMenuItem("Resize", "R", "R", null));
        add(new JSeparator());
        add(MenuUtils.createMenuItem("Delete", "D", "D", null));
    }

    private class CreateMenu extends JMenu {

        public CreateMenu() {
            super("Create");
            List<JMenuItem> items = new ArrayList<>();
            items.add(createMenuItem("Circle", "C", "0", null));
            items.add(createMenuItem("Line", "L", "1", null));
            items.add(createMenuItem("Rectangle", "R", "2", new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println("create rectangle");
                    outer.pPanel.addMouseListener(new DrawRectangleListener(outer.sMgr, outer.currentColor, outer.fillState));
                }
            }));
            items.add(createMenuItem("Ellipse", "E", "3", null));

            for (JMenuItem i : items) {
                add(i);
            }

        }
    }

}
