package at.htlpinkafeld.shapes.gui;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

class NewAction extends AbstractAction {
    private final ShapeFrame outer;

    public NewAction(final ShapeFrame outer) {
        super("New");
        this.outer = outer;
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        putValue(MNEMONIC_KEY, KeyEvent.VK_N);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        outer.sMgr.clear();
        outer.pPanel.repaint();
        System.out.println("File-New performed");
    }

}
