package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.utils.MenuUtils;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

class SettingsMenu extends JMenu {

    private final ShapeFrame outer;

    public SettingsMenu(final ShapeFrame outer) {
        super("Settings");
        this.outer = outer;
        JMenuItem colors = new JMenu("Color");
        List<JMenuItem> items = new ArrayList<>();
        
        items.add(new ColorChooserRadioButtonMenuItem("Red", "R", null, Color.red));

        items.add(new ColorChooserRadioButtonMenuItem("Green", "G", null, Color.green));

        items.add(new ColorChooserRadioButtonMenuItem("Blue", "B", null, Color.blue));

        ButtonGroup group = new ButtonGroup();
        for (JMenuItem i : items) {
            group.add(i);
            colors.add(i);
        }
        
        items.get(0).setSelected(true);
        outer.currentColor = Color.red;
        
        add(colors);
        JMenuItem fill = MenuUtils.createCheckBoxMenuItem("Filled", "F", "F", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                outer.fillState = ((JCheckBoxMenuItem) e.getSource()).getState();
            }
        });
        add(fill);
    }

    private class ColorChooserRadioButtonMenuItem extends JRadioButtonMenuItem {
        private Color c;

        public ColorChooserRadioButtonMenuItem(String text, String mnemonic, String accelerator, Color c) {
            super(text);
            this.c = c;
            MenuUtils.configureMenuItem(this, mnemonic, accelerator, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    outer.currentColor = c;
                }
            });
        }
        
    }
    
}
