package at.htlpinkafeld.shapes.gui;

import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WindowClosingAdapter extends WindowAdapter {

    private Component parent;
    
    Logger logger = LogManager.getLogger();

    public WindowClosingAdapter(Component parent) {
        this.parent = parent;
    }

    @Override
    public void windowClosing(WindowEvent event) {
        int ans = JOptionPane.showConfirmDialog(parent, "Finish program?", "Exit?", JOptionPane.YES_NO_OPTION);
        if (ans == JOptionPane.YES_OPTION) {
            event.getWindow().setVisible(false);
            event.getWindow().dispose();
            logger.warn("Program closed!");
            System.exit(0);
        }
    }
}
