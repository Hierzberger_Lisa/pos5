/*
 * StdinReader.java
 *
 * Created on 13. September 2002, 08:45
 */
package at.htlpinkafeld.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author  posch
 * @version 0.1
 *
 * Reads numbers and Strings from the stdInput
 */
public class StdinReader extends java.lang.Object {
    
    /** Creates new StdinReader */
    public StdinReader() {
    }
    
    /**
     * @param prompt the String which should prompt the user for input
     * @return the feeded value or 0 if its was no number
     */
    public static int readInt(String prompt) {
        BufferedReader dis = new BufferedReader(new InputStreamReader(System.in));
        String intString = null;
        int zahl = 0;
        System.out.print("\n" + prompt);
        try {
            intString = dis.readLine();
            zahl = Integer.parseInt(intString);
        }
        catch ( NumberFormatException e) {
            System.err.println("No Number");
        }
        catch (Exception e) {
            System.err.println(e);
        }
        
        return zahl;
    }
    
    /**
     * @param prompt the String which should prompt the user for input
     * @return the feeded string
     */
    public static String readString(String prompt) {
        BufferedReader dis = new BufferedReader(new InputStreamReader(System.in));
        String string = null;
        System.out.print("\n" + prompt);
        try {
            string = dis.readLine();
        }
        catch (Exception e) {
            System.err.println(e);
        }
        
        return string;
    }
    
    public static char readChar(String prompt) {
        String string = readString(prompt);
      
        return string.charAt(0);
    }
    
    /**
     * @param prompt the String which should prompt the user for input
     * @return the feeded float
     */
     public static float readFloat(String prompt) {
        BufferedReader dis = new BufferedReader(new InputStreamReader(System.in));
        String floatString;
        float zahl = 0;
        System.out.print("\n" + prompt);
        try {
            floatString = dis.readLine();
            zahl = Float.parseFloat(floatString);
        }
        
        catch ( NumberFormatException e) {
            System.err.println("No Number");
        }
        
        catch (Exception e) {
            System.err.println(e);
        }
        
        return zahl;
    }
    
    /**
     * @param prompt the String which should prompt the user for input
     * @return the feeded double
     */
     public static double readDouble(String prompt) {
        BufferedReader dis = new BufferedReader(new InputStreamReader(System.in));
        String doubleString;
        double zahl = 0;
        System.out.print("\n" + prompt);
        try {
            doubleString = dis.readLine();
            zahl = Double.parseDouble(doubleString);
        }
        
        catch ( NumberFormatException e) {
            System.err.println("No Number");
        }
        
        catch (Exception e) {
            System.err.println(e);
        }
        
        return zahl;
    }
}
