/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.observer;

/**
 *
 * @author maue
 */
public interface PropertyChangeListener {
   void propertyChange(PropertyChangeEvent evt);
}
