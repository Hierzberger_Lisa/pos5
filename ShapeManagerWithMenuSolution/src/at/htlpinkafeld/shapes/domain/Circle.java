/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.domain;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author maue
 */
public class Circle extends GeometricShape {

    //Propertienames for PropertyChangeListener
    public static final String PROP_RADIUS = "RADIUS";
    public static final String PROP_CENTER = "CENTER";

    private int rad;
    private Point center;

    Logger logger = LogManager.getLogger();

    public Circle(int cx, int cy, int rad, Color col, boolean filled) {
        super(col, filled);
        this.rad = rad;
        this.center = new Point(cx, cy);
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        int oldRad = this.rad;
        this.rad = rad;
        firePropertyChange(PROP_RADIUS, oldRad, this.rad);
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        Point oldPnt = new Point(this.center);
        this.center = center;
        firePropertyChange(PROP_CENTER, oldPnt, this.center);
    }

    @Override
    public void draw(Graphics g) {
        int x = this.center.x - this.rad;
        int y = this.center.y - this.rad;
        int wh = 2 * rad;

        g.setColor(this.getCol());
        if (this.isFilled()) {
            g.fillOval(x, y, wh, wh);
            logger.info("Filled circle created");
        } else {
            g.drawOval(x, y, wh, wh);
            logger.info("Unfilled circle created");
        }
    }

    @Override
    public Point getRefPoint() {
        return this.center;
    }

    @Override
    public boolean isInside(Point pnt) {
        logger.warn("IsInside");
        return pnt.distance(this.center) < this.rad;
    }

    @Override
    public void move(int byX, int byY) {
        setCenter(new Point(center.x + byX, center.y + byY));
        logger.info("Moving Circle ");
    }
}
