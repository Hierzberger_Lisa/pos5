/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.domain;

import at.htlpinkafeld.shapes.gui.ShapeFrame;
import at.htlpinkafeld.utils.StdinReader;
import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;

/**
 *
 * @author maue
 */
public class CreateAndDrawShapes {

   private static ShapeFrame shFrame;

   public static void main(String[] args) throws InterruptedException, InvocationTargetException {
      //      final ShapeFrame f;
      ShapeManager shapeMgr = new ShapeManager();
      int colIdx = 0;
      Color[] colArr = {Color.RED, Color.GREEN, Color.BLUE};
      String[] csArr = {"red",     "green",     "blue"};
      final int maxCol = colArr.length;
      char choice;
      boolean fill = false;
      GeometricShape actShape = null;
      //Rectangle actRect;

      SwingUtilities.invokeAndWait(new Runnable() {
         public void run() {
            shFrame = new ShapeFrame(shapeMgr);
         }
      });

      do {
         choice = drawMenue(csArr, colIdx, fill);
         switch (choice) {
            case 'f':
               fill = !fill;
               break;
            case 'n':
               colIdx++;
               colIdx %= maxCol;
               break;
            case 'c':
               actShape = getCircle(colArr[colIdx], fill);
               break;
            case 'r':
               actShape = getRect(colArr[colIdx], fill);
               break;
         }

         if (actShape != null) {
            shapeMgr.addShape(actShape);
            shFrame.repaint();
            actShape = null;
         }

      } while (choice != 'q');

      System.out.println("End of input!");
   }

   private static char drawMenue(String[] colNames, int idx, boolean fill) {
      System.out.println("Shape Creator 1.0 Beta");
      System.out.println("======================");
      System.out.println("");
      System.out.print("Actual Color:"); 
      for(String actStr: colNames ){
         if( actStr == colNames[idx] )
            actStr = actStr.toUpperCase();
         System.out.print("  "+actStr);
      }
      System.out.println("");        
      System.out.println("Fillmode: " + (fill?"ON":"OFF"));
      System.out.println("");
      System.out.println("n) Next color");
      System.out.println("f) Fill mode toggle");
      System.out.println("c) Circle creation");
      System.out.println("r) Rectangle creation");
      System.out.println("");
      System.out.println("q) Quit");
      System.out.println("");
      
      return StdinReader.readChar("Youre choice: ");       
   }

   private static GeometricShape getCircle(Color actCol, boolean actFill) {
         int cx, cy, rad;
         System.out.println("");
         System.out.println("");
         System.out.println("Circle creation");
         System.out.println("===============");
         cx = StdinReader.readInt("Center x-coord: ");
         cy = StdinReader.readInt("Center y-coord: ");
         rad = StdinReader.readInt("Radius: ");
  
         return new Circle(cx, cy, rad, actCol, actFill);
   }

   private static GeometricShape getRect(Color actCol, boolean actFill) {
         int ulx, uly, w, h;
         System.out.println("");
         System.out.println("");
         System.out.println("Rectangle creation");
         System.out.println("==================");
         ulx = StdinReader.readInt("Upperleft x-coord: ");
         uly = StdinReader.readInt("Upper left y-coord: ");
         w = StdinReader.readInt("Width: ");
         h = StdinReader.readInt("Height: ");
  
         return new Rectangle(ulx, uly, w, h, actCol, actFill);  
   }

}
