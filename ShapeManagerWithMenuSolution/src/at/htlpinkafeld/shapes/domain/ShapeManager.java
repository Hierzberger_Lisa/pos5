/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.domain;

import at.htlpinkafeld.observer.PropertyChangeListener;
import at.htlpinkafeld.observer.ShapeChangeListener;
import java.util.*;
import javax.swing.JFrame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author maue
 */
public class ShapeManager implements Iterable<GeometricShape>{
   private List<GeometricShape> shapeList = new LinkedList<>();
   private ShapeChangeListener shapeChangeListener = new ShapeChangeListener();
   
   Logger logger = LogManager.getLogger();
   
   public ShapeManager(){ }
   
   public void linkFrame(JFrame fr){
      shapeChangeListener.setFrame(fr);
   }
   
   public void addShape(GeometricShape sh){
      if( this.shapeChangeListener != null)
         sh.addPropertyChangeListener(this.shapeChangeListener);
      shapeList.add(sh);
   }
   
   public GeometricShape getShape(int idx){
      return shapeList.get(idx);
   }
   
   public void removeShape(int idx){
      GeometricShape sh;
      sh = this.getShape(idx);
      sh.removePropertyChangeListener(this.shapeChangeListener);
      shapeList.remove(sh);
   }

   @Override
   public Iterator<GeometricShape> iterator() {
      return shapeList.iterator();
   }

    public void clear() {
        for(GeometricShape s : shapeList) {
            s.removePropertyChangeListener(this.shapeChangeListener);
            logger.warn("Shapes deleted!!");
        }
        shapeList.clear();
    }

}
