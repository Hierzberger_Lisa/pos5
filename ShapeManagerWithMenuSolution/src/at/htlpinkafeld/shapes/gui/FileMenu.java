package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.utils.MenuUtils;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

class FileMenu extends JMenu {
    private final ShapeFrame outer;

    public FileMenu(final ShapeFrame outer) {
        super("File");
        this.outer = outer;
        setMnemonic(KeyEvent.VK_F);
        // example for use of Actions
        add(new JMenuItem(new NewAction(outer)));
        add(MenuUtils.createMenuItem("Open", "O", "O", null));
        add(MenuUtils.createMenuItem("Save", "S", "S", null));
        add(MenuUtils.createMenuItem("Exit", "X", "Q", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WindowEvent closingEvent = new WindowEvent(outer, WindowEvent.WINDOW_CLOSING);
                Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closingEvent);
            }
        }));
    }

}
