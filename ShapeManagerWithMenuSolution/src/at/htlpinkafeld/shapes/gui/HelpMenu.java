package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.utils.MenuUtils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

class HelpMenu extends JMenu {
    private final ShapeFrame outer;

    public HelpMenu(final ShapeFrame outer) {
        super("Help");
        this.outer = outer;
        add(MenuUtils.createMenuItem("Info", "I", "I", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(outer, "(c) Thomas Quaritsch blablabla", "About ShapeManager", JOptionPane.PLAIN_MESSAGE);
            }
        }));
    }

}
