/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.shapes.domain.ShapeManager;
import at.htlpinkafeld.shapes.domain.GeometricShape;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author maue
 */
public class PaintPanel extends JPanel {
   private ShapeManager shapeMgr;

   public PaintPanel(ShapeManager sMgr) {
      this.shapeMgr = sMgr;
   }
   
   @Override
   public void paintComponent(Graphics g){
      for(GeometricShape shape: shapeMgr) 
         shape.draw(g);
   } 
}
