/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.shapes.gui;

import at.htlpinkafeld.shapes.domain.ShapeManager;
import static at.htlpinkafeld.utils.MenuUtils.createCheckBoxMenuItem;
import static at.htlpinkafeld.utils.MenuUtils.createMenuItem;
import static at.htlpinkafeld.utils.MenuUtils.createRadioButtonMenuItem;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

/**
 *
 * @author maue
 */
public class ShapeFrame extends JFrame {

    protected PaintPanel pPanel;
    protected ShapeManager sMgr;
    protected Color currentColor;
    protected Boolean fillState = false;

    public ShapeFrame(ShapeManager sMgr) {
        super("Super Duper 2D Drawing Program");

        this.sMgr = sMgr;
        sMgr.linkFrame(this);
        pPanel = new PaintPanel(sMgr);
        this.setContentPane(pPanel);

        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        menuBar.add(new FileMenu(this));
        menuBar.add(new EditMenu(this));
        menuBar.add(new SettingsMenu(this));
        menuBar.add(new HelpMenu(this));

        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowClosingAdapter(this));
        this.setLocation(100, 100);
        this.setSize(500, 500);
        this.setVisible(true);
    }


}
