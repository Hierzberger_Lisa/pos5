package at.htlpinkafeld.utils;

import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

public class MenuUtils {

    public static JMenuItem createRadioButtonMenuItem(String text, String mnemonic, String accelerator, ActionListener a) {
        JMenuItem menuItem = new JRadioButtonMenuItem(text);
        configureMenuItem(menuItem, mnemonic, accelerator, a);
        return menuItem;
    }

    public static JMenuItem createCheckBoxMenuItem(String text, String mnemonic, String accelerator, ActionListener a) {
        JMenuItem menuItem = new JCheckBoxMenuItem(text);
        configureMenuItem(menuItem, mnemonic, accelerator, a);
        return menuItem;
    }

    public static JMenuItem createMenuItem(String text, String mnemonic, String accelerator, ActionListener a) {
        JMenuItem menuItem = new JMenuItem(text);
        configureMenuItem(menuItem, mnemonic, accelerator, a);
        return menuItem;
    }

    public static void configureMenuItem(JMenuItem menuItem, String mnemonic, String accelerator, ActionListener a) {
        if (mnemonic != null) {
            menuItem.setMnemonic(KeyEvent.getExtendedKeyCodeForChar(mnemonic.charAt(0)));
        }
        if (accelerator != null) {
            menuItem.setAccelerator(KeyStroke.getKeyStroke(accelerator.charAt(0), InputEvent.CTRL_DOWN_MASK));
        }
        if (a != null) {
            menuItem.addActionListener(a);
        }
    }

}
