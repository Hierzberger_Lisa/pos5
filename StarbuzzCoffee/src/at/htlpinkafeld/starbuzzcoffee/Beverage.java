package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */


//Component
public interface Beverage {
    public double cost();
    public String getDescription();

}
