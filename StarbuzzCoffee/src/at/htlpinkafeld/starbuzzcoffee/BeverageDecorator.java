package at.htlpinkafeld.starbuzzcoffee;
/**
 *
 * @author lisa
 */


//Decorator
public abstract class BeverageDecorator implements Beverage {

    private Beverage b;

    public BeverageDecorator(Beverage b) {
        this.b = b;
    }

    @Override
    public double cost() {
        return b.cost();
    }

    @Override
    public String getDescription() {
        return b.getDescription();
    }
    
    

}
