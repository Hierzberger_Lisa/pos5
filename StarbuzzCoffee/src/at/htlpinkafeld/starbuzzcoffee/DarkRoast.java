package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Component
public final class DarkRoast implements Beverage{

    @Override
    public double cost() {
        return 0.99;
    }

    @Override
    public String getDescription() {
        return "Dark Roast";
    }
    
}
