package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Component
public final class Decaf implements Beverage{

    @Override
    public double cost() {
        return 1.05;
    }

    @Override
    public String getDescription() {
        return "Decaf";
    }
    
}
