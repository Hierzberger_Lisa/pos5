package at.htlpinkafeld.starbuzzcoffee;
/**
 *
 * @author lisa
 */

//Concreate Component
public final class Espresso implements Beverage{

    @Override
    public double cost() {
        return 1.99;
    }

    @Override
    public String getDescription() {
        return "Espresso";
    }

    
    
}
