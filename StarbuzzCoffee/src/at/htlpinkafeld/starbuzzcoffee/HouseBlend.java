package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Component
public final class HouseBlend implements Beverage{

    @Override
    public double cost() {
        return 0.89;
    }
 
    @Override
    public String getDescription() {
        return "House Blend";
    }
    
}
