package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Decorator
public final class Milk extends BeverageDecorator {

    public Milk(Beverage b) {
        super(b);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Milk";
    }

    @Override
    public double cost() {
        return super.cost() + 0.10;
    }

}
