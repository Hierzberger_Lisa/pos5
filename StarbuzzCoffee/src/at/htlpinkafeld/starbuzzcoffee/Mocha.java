package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Decorator
public final class Mocha extends BeverageDecorator {

    public Mocha(Beverage b) {
        super(b);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Mocha";
    }

    @Override
    public double cost() {
        return super.cost() + 0.2;
    }

}
