package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */
//Concreate Decorator
public final class Soy extends BeverageDecorator{
    
    public Soy(Beverage b) {
        super(b);
    }
    
    @Override
    public String getDescription() {
        return super.getDescription() + ", Soy"; 
    }

    @Override
    public double cost() {
        return super.cost() + 0.15;
    }
    
}
