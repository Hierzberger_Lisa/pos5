package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */
//Main
public class StarbuzzCoffee {

    public static void main(String[] args) {
        //Just an espresso, no condiments
        Beverage beverage = new Espresso();
        System.out.println(beverage.getDescription() + " €" + beverage.cost());

        //Dark Roast with double Mocha and Whip
        Beverage beverage2 = new DarkRoast();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.getDescription() + " €" + beverage2.cost());

        //House Blend with Soy, Mocha and Whip
        //Nested construcor calls 
        Beverage beverage3 = new Whip(new Mocha(new Soy(new HouseBlend())));
        System.out.println(beverage3.getDescription() + " €" + beverage3.cost());
        
        Beverage b = new Milk( new DarkRoast());
        b.cost();
    }

}
