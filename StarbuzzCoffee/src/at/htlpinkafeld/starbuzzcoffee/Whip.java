package at.htlpinkafeld.starbuzzcoffee;

/**
 *
 * @author lisa
 */

//Concreate Decorator
public final class Whip extends BeverageDecorator {

    public Whip(Beverage b) {
        super(b);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ", Whip";
    }

    @Override
    public double cost() {
        return super.cost() + 0.1;
    }

}
