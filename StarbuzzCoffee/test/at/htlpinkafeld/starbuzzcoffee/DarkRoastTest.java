package at.htlpinkafeld.starbuzzcoffee;


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class DarkRoastTest {
    private Beverage b;

    /** Test of cost method, of class DarkRoast. */
    @Test
    public void testCost() {
        assertEquals(0.99, this.b.cost(), 0.01);
    }

    /**Test of getIngredients method, of class DarkRoast. */
    @Test
    public void testGetIngredients() {
        String s = "Dark Roast";
        assertEquals(s, this.b.getDescription());

    }
    
}
