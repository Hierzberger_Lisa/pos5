package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class DecafTest {

    Beverage b;

    /**
     * Test of cost method, of class Decaf.
     */
    @Test
    public void testCost() {
        assertEquals(1.05, this.b.cost(), 0.01);
    }

    /**
     * Test of getIngredients method, of class Decaf.
     */
    @Test
    public void testGetIngredients() {
        String s = "Decaf";
        assertEquals(s, this.b.getDescription());
    }

}
