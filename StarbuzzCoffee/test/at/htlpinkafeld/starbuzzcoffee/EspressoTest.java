package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class EspressoTest {
    Beverage b;
    
    public EspressoTest() {
    }
    
    @Before
    public void setUp() {
        this.b = new Espresso();
    }

    /** Test of cost method, of class Espresso. */
    @Test
    public void testCost() {
        assertEquals(1.99, this.b.cost(), 0.01);
    }

    /** Test of getIngredients method, of class Espresso. */
    @Test
    public void testgetDescription() {
        String s = "Espresso";
        
        assertEquals(s, this.b.getDescription());
    }
    
}
