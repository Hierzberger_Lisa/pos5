package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class HouseBlendTest {
    Beverage b;
    
    public HouseBlendTest() {
    }
    
    @Before
    public void setUp() {
        this.b = new HouseBlend();
    }

    /** Test of cost method, of class HouseBlend. */
    @Test
    public void testCost() {
        assertEquals(0.89, this.b.cost(), 0.01);
    }

    /** Test of getIngredients method, of class Espresso.*/
    @Test
    public void testgetDescription() {
        String s = "House Blend";       
        assertEquals(s, this.b.getDescription());
    } 
}
