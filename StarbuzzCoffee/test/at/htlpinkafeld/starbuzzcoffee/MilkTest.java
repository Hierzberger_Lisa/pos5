package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class MilkTest {

    Beverage b;

    public MilkTest() {
    }

    @Before
    public void setUp() {
        this.b = new Milk(new DarkRoast());
    }

    /**
     * Test of getIngredients method, of class Milk.
     */
    @Test
    public void testgetDescription() {
        String s = "Dark Roast, Milk";
        assertEquals(s, this.b.getDescription());
    }

    /**
     * Test of cost method, of class Milk.
     */
    @Test
    public void testCost() {
        assertEquals(1.09, this.b.cost(), 0.01);
    }

    
    @Test
    public void testCostMilkAddsTenCents() {
        class TestBeverage implements Beverage {

            private final static double TEST_COST = 1.23;

            @Override
            public String getDescription() {
                return "fake test beverage";
            }

            @Override
            public double cost() {
                return TEST_COST;
            }
        }
        TestBeverage b = new TestBeverage();
        Milk m = new Milk(b);
        assertEquals(TestBeverage.TEST_COST + 0.1, m.cost(), 1e-6);
    }
}
