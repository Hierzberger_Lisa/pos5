package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class MochaTest {

    private Beverage b;

    public MochaTest() {
    }

    @Before
    public void setUp() {
        this.b = new Mocha(new DarkRoast());
    }

    /**
     * Test of getIngredients method, of class Mocha.
     */
    @Test
    public void testgetDescription() {
        String s = "Dark Roast, Mocha";
        assertEquals(s, this.b.getDescription());
    }

    /**
     * Test of cost method, of class Milk.
     */
    @Test
    public void testCost() {
        assertEquals(1.19, this.b.cost(), 0.01);
    }

    @Test
    public void testCostMochaAdds20Cents() {
        class TestBeverage implements Beverage {

            private final static double TEST_COST = 1.00;

            @Override
            public String getDescription() {
                return "fake test beverage";
            }

            @Override
            public double cost() {
                return TEST_COST;
            }
        }
        TestBeverage b = new TestBeverage();
        Mocha m = new Mocha(b);
        assertEquals(TestBeverage.TEST_COST + 0.2, m.cost(), 1e-6);
    }

}
