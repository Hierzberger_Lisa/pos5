package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class SoyTest {
    private Beverage b;
    
    public SoyTest() {
    }
    
    @Before
    public void setUp() {
        this.b = new Soy(new DarkRoast());
    }

    /** Test of getIngredients method, of class Soy. */
    @Test
    public void testgetDescription() {
        String s = "Dark Roast, Soy";
        assertEquals(s, this.b.getDescription());
    }

    /** Test of cost method, of class Milk. */
    @Test
    public void testCost() {
        assertEquals(1.14, this.b.cost(), 0.01);
    } 
    
    
    @Test
    public void testCostSoyAdds15Cents() {
        class TestBeverage implements Beverage {

            private final static double TEST_COST = 1.50;

            @Override
            public String getDescription() {
                return "fake test beverage";
            }

            @Override
            public double cost() {
                return TEST_COST;
            }
        }
        TestBeverage b = new TestBeverage();
        Soy s = new Soy(b);
        assertEquals(TestBeverage.TEST_COST + 0.15, s.cost(), 1e-6);
    }
}
