package at.htlpinkafeld.starbuzzcoffee;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lisa
 */
public class WhipTest {

    Beverage b;

    public WhipTest() {
    }

    @Before
    public void setUp() {
        this.b = new Whip(new DarkRoast());
    }

    /**
     * Test of getIngredients method, of class Whip.
     */
    @Test
    public void testgetDescription() {
        String s = "Dark Roast, Whip";
        assertEquals(s, this.b.getDescription());
    }

    /**
     * Test of cost method, of class Milk.
     */
    @Test
    public void testCost() {
        assertEquals(1.09, this.b.cost(), 0.01);
    }

    @Test
    public void testCostWhipAdds10Cents() {
        class TestBeverage implements Beverage {

            private final static double TEST_COST = 1.00;

            @Override
            public String getDescription() {
                return "fake test beverage";
            }

            @Override
            public double cost() {
                return TEST_COST;
            }
        }
        TestBeverage b = new TestBeverage();
        Whip w = new Whip(b);
        assertEquals(TestBeverage.TEST_COST + 0.1, w.cost(), 1e-6);
    }

}
